<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="ru">
<context>
    <name>Community</name>
    <message>
        <source>We use Stomt because it provides quick and easy feedback via I like/I wish. So you can easily give us feedback and speak your mind. We will read these wishes on a daily basis!</source>
        <translation type="obsolete">Мы используем Stomt, потому что он обеспечивает быструю и легкую обратную связь через &quot;Мне нравится / Я бы хотел&quot;. Таким образом, вы можете легко дать нам обратную связь и высказать свое мнение. Мы будем читать эти пожелания ежедневно!</translation>
    </message>
    <message>
        <source>Open ScreenPlay Stomt page</source>
        <translation type="vanished">Открыть страницу ScreenPlay Stomt</translation>
    </message>
    <message>
        <source>Forums</source>
        <translation type="obsolete">Форум</translation>
    </message>
    <message>
        <source>Blog</source>
        <translation type="obsolete">Блог</translation>
    </message>
    <message>
        <source>Source Code</source>
        <translation type="obsolete">Исходный код</translation>
    </message>
    <message>
        <source>Workshop</source>
        <translation type="obsolete">Мастерская</translation>
    </message>
    <message>
        <location filename="../qml/Community/Community.qml" line="34"/>
        <source>Wiki</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/Community/Community.qml" line="40"/>
        <source>Forum</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/Community/Community.qml" line="45"/>
        <source>Issue List</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/Community/Community.qml" line="50"/>
        <source>Release Notes</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/Community/Community.qml" line="55"/>
        <source>Contribution Guide</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/Community/Community.qml" line="60"/>
        <source>Steam Workshop</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>CommunityNavItem</name>
    <message>
        <location filename="../qml/Community/CommunityNavItem.qml" line="59"/>
        <source>Open in browser</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>Create</name>
    <message>
        <location filename="../qml/Create/Create.qml" line="101"/>
        <source>Create wallpapers and widgets for local usage or the steam workshop!</source>
        <translation type="unfinished">Создавайте обои и виджеты для персонального использования или для мастерской Steam!</translation>
    </message>
</context>
<context>
    <name>CreateContent</name>
    <message>
        <location filename="../qml/Create/CreateContent.qml" line="15"/>
        <source>Create Widgets and Scenes</source>
        <translation type="unfinished">Создать Виджеты и Сцены</translation>
    </message>
    <message>
        <source>Examples Widgets and Scenes</source>
        <translation type="obsolete">Примеры виджетов и сцен</translation>
    </message>
    <message>
        <location filename="../qml/Create/CreateContent.qml" line="29"/>
        <source>Create Empty Widget</source>
        <translation type="unfinished">Создать пустой виджет</translation>
    </message>
    <message>
        <location filename="../qml/Create/CreateContent.qml" line="42"/>
        <source>Example Widgets and Scenes</source>
        <translation type="unfinished">Примеры виджетов и сцен</translation>
    </message>
    <message>
        <location filename="../qml/Create/CreateContent.qml" line="65"/>
        <source>Empty HTML Wallpaper</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Simple clock widget</source>
        <translation type="obsolete">Простой виджет часов</translation>
    </message>
    <message>
        <location filename="../qml/Create/CreateContent.qml" line="75"/>
        <source>Musik scene wallpaper visualizer</source>
        <translation type="unfinished">Визуализатор сцены обоев Musik</translation>
    </message>
    <message>
        <location filename="../qml/Create/CreateContent.qml" line="86"/>
        <source>Changing scene wallpaper via unsplash.com</source>
        <translation type="unfinished">Изменение сцены обоев через unsplash.com</translation>
    </message>
</context>
<context>
    <name>CreateContentButton</name>
    <message>
        <location filename="../qml/Create/CreateContentButton.qml" line="114"/>
        <source>Not yet implemented. Stay tuned!</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>CreateEmptyHtmlWallpaper</name>
    <message>
        <location filename="../qml/Create/Wizards/CreateEmptyHtmlWallpaper/CreateEmptyHtmlWallpaper.qml" line="54"/>
        <source>This wizard lets you create a empty html based wallpaper. You can put anything you can imagine into this html file. For example this can be a three.js scene or a utility application written in javascript.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/Create/Wizards/CreateEmptyHtmlWallpaper/CreateEmptyHtmlWallpaper.qml" line="68"/>
        <source>Next</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/Create/Wizards/CreateEmptyHtmlWallpaper/CreateEmptyHtmlWallpaper.qml" line="119"/>
        <source>Create a html Wallpaper</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/Create/Wizards/CreateEmptyHtmlWallpaper/CreateEmptyHtmlWallpaper.qml" line="123"/>
        <source>General</source>
        <translation type="unfinished">Общие сведения</translation>
    </message>
    <message>
        <location filename="../qml/Create/Wizards/CreateEmptyHtmlWallpaper/CreateEmptyHtmlWallpaper.qml" line="131"/>
        <source>Wallpaper name</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/Create/Wizards/CreateEmptyHtmlWallpaper/CreateEmptyHtmlWallpaper.qml" line="145"/>
        <source>Copyright owner</source>
        <translation type="unfinished">Правообладатель</translation>
    </message>
    <message>
        <location filename="../qml/Create/Wizards/CreateEmptyHtmlWallpaper/CreateEmptyHtmlWallpaper.qml" line="149"/>
        <source>License</source>
        <translation type="unfinished">Лицензия</translation>
    </message>
    <message>
        <location filename="../qml/Create/Wizards/CreateEmptyHtmlWallpaper/CreateEmptyHtmlWallpaper.qml" line="172"/>
        <source>Tags</source>
        <translation type="unfinished">Теги</translation>
    </message>
    <message>
        <location filename="../qml/Create/Wizards/CreateEmptyHtmlWallpaper/CreateEmptyHtmlWallpaper.qml" line="184"/>
        <source>Preview Image</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/Create/Wizards/CreateEmptyHtmlWallpaper/CreateEmptyHtmlWallpaper.qml" line="191"/>
        <source>You can set your own preview image here!</source>
        <translation type="unfinished">Вы можете установить свой собственный предварительный просмотр изображения здесь!</translation>
    </message>
    <message>
        <location filename="../qml/Create/Wizards/CreateEmptyHtmlWallpaper/CreateEmptyHtmlWallpaper.qml" line="216"/>
        <source>Abort</source>
        <translation type="unfinished">Отменить</translation>
    </message>
    <message>
        <location filename="../qml/Create/Wizards/CreateEmptyHtmlWallpaper/CreateEmptyHtmlWallpaper.qml" line="228"/>
        <source>Save</source>
        <translation type="unfinished">Сохранить</translation>
    </message>
    <message>
        <location filename="../qml/Create/Wizards/CreateEmptyHtmlWallpaper/CreateEmptyHtmlWallpaper.qml" line="263"/>
        <source>Create Html Wallpaper...</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>CreateEmptyWidget</name>
    <message>
        <location filename="../qml/Create/Wizards/CreateEmptyWidget/CreateEmptyWidget.qml" line="17"/>
        <source>Create an empty widget</source>
        <translation type="unfinished">Создать пустой виджет</translation>
    </message>
    <message>
        <location filename="../qml/Create/Wizards/CreateEmptyWidget/CreateEmptyWidget.qml" line="77"/>
        <source>General</source>
        <translation type="unfinished">Общие сведения</translation>
    </message>
    <message>
        <location filename="../qml/Create/Wizards/CreateEmptyWidget/CreateEmptyWidget.qml" line="85"/>
        <source>Widget name</source>
        <translation type="unfinished">Имя виджета</translation>
    </message>
    <message>
        <location filename="../qml/Create/Wizards/CreateEmptyWidget/CreateEmptyWidget.qml" line="98"/>
        <source>Copyright owner</source>
        <translation type="unfinished">Правообладатель</translation>
    </message>
    <message>
        <location filename="../qml/Create/Wizards/CreateEmptyWidget/CreateEmptyWidget.qml" line="102"/>
        <source>Type</source>
        <translation type="unfinished">Тип</translation>
    </message>
    <message>
        <location filename="../qml/Create/Wizards/CreateEmptyWidget/CreateEmptyWidget.qml" line="131"/>
        <source>License</source>
        <translation type="unfinished">Лицензия</translation>
    </message>
    <message>
        <location filename="../qml/Create/Wizards/CreateEmptyWidget/CreateEmptyWidget.qml" line="154"/>
        <source>Tags</source>
        <translation type="unfinished">Теги</translation>
    </message>
    <message>
        <location filename="../qml/Create/Wizards/CreateEmptyWidget/CreateEmptyWidget.qml" line="178"/>
        <source>Save</source>
        <translation type="unfinished">Сохранить</translation>
    </message>
    <message>
        <location filename="../qml/Create/Wizards/CreateEmptyWidget/CreateEmptyWidget.qml" line="205"/>
        <source>Abort</source>
        <translation type="unfinished">Отменить</translation>
    </message>
    <message>
        <location filename="../qml/Create/Wizards/CreateEmptyWidget/CreateEmptyWidget.qml" line="233"/>
        <source>Create Widget...</source>
        <translation type="unfinished">Создать Виджет...</translation>
    </message>
</context>
<context>
    <name>CreateImport</name>
    <message>
        <location filename="../qml/Create/CreateImport.qml" line="137"/>
        <source></source>
        <translation></translation>
    </message>
</context>
<context>
    <name>CreateUpload</name>
    <message>
        <location filename="../qml/Create/CreateUpload.qml" line="219"/>
        <source></source>
        <translation></translation>
    </message>
</context>
<context>
    <name>CreateWallpaperCodec</name>
    <message>
        <location filename="../qml/Create/Wizards/CreateWallpaper/CreateWallpaperCodec.qml" line="52"/>
        <source>Import a video</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/Create/Wizards/CreateWallpaper/CreateWallpaperCodec.qml" line="62"/>
        <source>Depending on your PC configuration it is better to convert your wallpaper to a specific video codec. If both have bad performance you can also try a QML wallpaper!</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/Create/Wizards/CreateWallpaper/CreateWallpaperCodec.qml" line="76"/>
        <source>Set your preffered video codec:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/Create/Wizards/CreateWallpaper/CreateWallpaperCodec.qml" line="108"/>
        <source>Open Documentation</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/Create/Wizards/CreateWallpaper/CreateWallpaperCodec.qml" line="125"/>
        <source>Next</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>CreateWallpaperResult</name>
    <message>
        <location filename="../qml/Create/Wizards/CreateWallpaper/CreateWallpaperResult.qml" line="18"/>
        <source>An error occurred!</source>
        <translation type="unfinished">Произошла ошибка!</translation>
    </message>
    <message>
        <location filename="../qml/Create/Wizards/CreateWallpaper/CreateWallpaperResult.qml" line="78"/>
        <source>Copy text to clipboard</source>
        <translation type="unfinished">Скопировать текст в буфер обмена</translation>
    </message>
    <message>
        <location filename="../qml/Create/Wizards/CreateWallpaper/CreateWallpaperResult.qml" line="88"/>
        <source>Back to create and send an error report!</source>
        <translation type="unfinished">Назад к созданию и отправке отчета об ошибке!</translation>
    </message>
</context>
<context>
    <name>CreateWallpaperVideoImportConvert</name>
    <message>
        <location filename="../qml/Create/Wizards/CreateWallpaper/CreateWallpaperVideoImportConvert.qml" line="54"/>
        <source>Generating preview image...</source>
        <translation type="unfinished">Генерирование изображения предварительного просмотра...</translation>
    </message>
    <message>
        <location filename="../qml/Create/Wizards/CreateWallpaper/CreateWallpaperVideoImportConvert.qml" line="57"/>
        <source>Generating preview thumbnail image...</source>
        <translation type="unfinished">Генерация эскизного изображения предварительного просмотра...</translation>
    </message>
    <message>
        <location filename="../qml/Create/Wizards/CreateWallpaper/CreateWallpaperVideoImportConvert.qml" line="64"/>
        <source>Generating 5 second preview video...</source>
        <translation type="unfinished">Генерация 5-секундного видео для предварительного просмотра...</translation>
    </message>
    <message>
        <location filename="../qml/Create/Wizards/CreateWallpaper/CreateWallpaperVideoImportConvert.qml" line="67"/>
        <source>Generating preview gif...</source>
        <translation type="unfinished">Генерирование предварительного просмотра gif-анимации...</translation>
    </message>
    <message>
        <location filename="../qml/Create/Wizards/CreateWallpaper/CreateWallpaperVideoImportConvert.qml" line="76"/>
        <source>Converting Audio...</source>
        <translation type="unfinished">Преобразование Аудио...</translation>
    </message>
    <message>
        <location filename="../qml/Create/Wizards/CreateWallpaper/CreateWallpaperVideoImportConvert.qml" line="79"/>
        <source>Converting Video... This can take some time!</source>
        <translation type="unfinished">Конвертирование видео... Это может занять некоторое время!</translation>
    </message>
    <message>
        <location filename="../qml/Create/Wizards/CreateWallpaper/CreateWallpaperVideoImportConvert.qml" line="83"/>
        <source>Converting Video ERROR!</source>
        <translation type="unfinished">Конвертирование не удалось!</translation>
    </message>
    <message>
        <location filename="../qml/Create/Wizards/CreateWallpaper/CreateWallpaperVideoImportConvert.qml" line="86"/>
        <source>Analyse Video ERROR!</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/Create/Wizards/CreateWallpaper/CreateWallpaperVideoImportConvert.qml" line="114"/>
        <source>Convert a video to a wallpaper</source>
        <translation type="unfinished">Конвертировать видео в видео обои</translation>
    </message>
    <message>
        <location filename="../qml/Create/Wizards/CreateWallpaper/CreateWallpaperVideoImportConvert.qml" line="164"/>
        <source></source>
        <translation></translation>
    </message>
    <message>
        <location filename="../qml/Create/Wizards/CreateWallpaper/CreateWallpaperVideoImportConvert.qml" line="211"/>
        <source>Generating preview video...</source>
        <translation type="unfinished">Генерация видео для предварительного просмотра...</translation>
    </message>
    <message>
        <location filename="../qml/Create/Wizards/CreateWallpaper/CreateWallpaperVideoImportConvert.qml" line="224"/>
        <source>You can set your own preview image here!</source>
        <translation type="unfinished">Вы можете установить свой собственный предварительный просмотр изображения здесь!</translation>
    </message>
    <message>
        <location filename="../qml/Create/Wizards/CreateWallpaper/CreateWallpaperVideoImportConvert.qml" line="258"/>
        <source>Name (required!)</source>
        <translation type="unfinished">Имя (требуется!)</translation>
    </message>
    <message>
        <location filename="../qml/Create/Wizards/CreateWallpaper/CreateWallpaperVideoImportConvert.qml" line="273"/>
        <source>Description</source>
        <translation type="unfinished">Описание</translation>
    </message>
    <message>
        <location filename="../qml/Create/Wizards/CreateWallpaper/CreateWallpaperVideoImportConvert.qml" line="281"/>
        <source>Youtube URL</source>
        <translation type="unfinished">Ссылка на Youtube</translation>
    </message>
    <message>
        <location filename="../qml/Create/Wizards/CreateWallpaper/CreateWallpaperVideoImportConvert.qml" line="309"/>
        <source>Abort</source>
        <translation type="unfinished">Отменить</translation>
    </message>
    <message>
        <location filename="../qml/Create/Wizards/CreateWallpaper/CreateWallpaperVideoImportConvert.qml" line="322"/>
        <source>Save</source>
        <translation type="unfinished">Сохранить</translation>
    </message>
    <message>
        <location filename="../qml/Create/Wizards/CreateWallpaper/CreateWallpaperVideoImportConvert.qml" line="359"/>
        <source>Save Wallpaper...</source>
        <translation type="unfinished">Сохранить обои...</translation>
    </message>
</context>
<context>
    <name>DefaultVideoControls</name>
    <message>
        <location filename="../qml/Monitors/DefaultVideoControls.qml" line="41"/>
        <source>Volume</source>
        <translation type="unfinished">Звук</translation>
    </message>
    <message>
        <location filename="../qml/Monitors/DefaultVideoControls.qml" line="55"/>
        <source>Playback rate</source>
        <translation type="unfinished">Скорость воспроизведения</translation>
    </message>
    <message>
        <location filename="../qml/Monitors/DefaultVideoControls.qml" line="67"/>
        <source>Current Video Time</source>
        <translation type="unfinished">Текущее время видео</translation>
    </message>
    <message>
        <location filename="../qml/Monitors/DefaultVideoControls.qml" line="85"/>
        <source>Fill Mode</source>
        <translation type="unfinished">Режим наполнения</translation>
    </message>
    <message>
        <location filename="../qml/Monitors/DefaultVideoControls.qml" line="110"/>
        <source>Stretch</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/Monitors/DefaultVideoControls.qml" line="113"/>
        <source>Fill</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/Monitors/DefaultVideoControls.qml" line="116"/>
        <source>Contain</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/Monitors/DefaultVideoControls.qml" line="119"/>
        <source>Cover</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/Monitors/DefaultVideoControls.qml" line="122"/>
        <source>Scale_Down</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>FFMPEGPopup</name>
    <message>
        <source>Begin downloading FFMPEG</source>
        <translation type="obsolete">Начать загрузку FFMPEG</translation>
    </message>
    <message>
        <source>FFMPEG download failed</source>
        <translation type="obsolete">Не удалось скачать FFMPEG</translation>
    </message>
    <message>
        <source>FFMPEG download successful</source>
        <translation type="obsolete">FFMPEG скачан успешно</translation>
    </message>
    <message>
        <source>Extracting FFMPEG</source>
        <translation type="obsolete">Извлечение FFMPEG</translation>
    </message>
    <message>
        <source>ERROR extracting ffmpeg from RAM</source>
        <translation type="obsolete">ОШИБКА извлечения ffmpeg из оперативной памяти</translation>
    </message>
    <message>
        <source>ERROR extracing ffmpeg</source>
        <translation type="obsolete">ОШИБКА распаковки ffmpeg</translation>
    </message>
    <message>
        <source>ERROR saving FFMPEG to disk</source>
        <translation type="obsolete">ОШИБКА сохранения FFMPEG на диск</translation>
    </message>
    <message>
        <source>ERROR extracing FFPROBE</source>
        <translation type="obsolete">ОШИБКА распаковки FFPROBE</translation>
    </message>
    <message>
        <source>ERROR saving FFPROBE to disk</source>
        <translation type="obsolete">Сохранение ОШИБКИ FFPROBE на диск</translation>
    </message>
    <message>
        <source>Extraction successful</source>
        <translation type="obsolete">Распаковка прошла успешно</translation>
    </message>
    <message>
        <source>All done and ready to go!</source>
        <translation type="obsolete">Все готово!</translation>
    </message>
    <message>
        <source>You cannot create Wallaper without FFMPEG installed!</source>
        <translation type="obsolete">Вы не сможете создать обои без установленного FFMPEG!</translation>
    </message>
    <message>
        <source>Abort</source>
        <translation type="obsolete">Отменить</translation>
    </message>
    <message>
        <source>Download FFMPEG</source>
        <translation type="obsolete">Скачать FFMPEG</translation>
    </message>
    <message>
        <source>Before we can start creating content you need to download FFMPEG</source>
        <translation type="obsolete">Перед тем, как мы начнем создавать контент, вам необходимо скачать FFMPEG</translation>
    </message>
    <message>
        <source>&lt;b&gt;Why do we bother you with this?&lt;/b&gt;
&lt;br&gt;&lt;br&gt; Well its because of &lt;b&gt;copyright&lt;/b&gt; and many many &lt;b&gt;patents&lt;/b&gt;.
Files like .mp4 or .webm are containers for video and audio. Every audio
and video file is encoded with a certain codec. These can be open source
ceand free to use like &lt;a href=&apos;https://wikipedia.org/wiki/VP8&apos;&gt;VP8&lt;/a&gt; and the newer  &lt;a href=&apos;https://wikipedia.org/wiki/VP9&apos;&gt;VP9&lt;/a&gt; (the one YouTube uses for their web
ms)but there are also some proprietary ones like  &lt;a href=&apos;https://wikipedia.org/wiki/H.264/MPEG-4_AVC&apos;&gt;h264&lt;/a&gt; and the successor &lt;a href=&apos;https://wikipedia.org/wiki/High_Efficiency_Video_Coding&apos;&gt;h265&lt;/a&gt;.
&lt;br&gt;
&lt;br&gt;


 We as software developer now need to deal with stuff like this in a field wedo not have any expertise in. The desicion to enable only free codecs for content wasan easy one but we still need to provide a way for our user to import wallpaper without a hassle. We do not provide  &lt;a href=&apos;https://ffmpeg.org/&apos;&gt;FFMPEG&lt;/a&gt; for converting video and audio with ScreenPlay because we are not allowed to. We let the user download &lt;a href=&apos;https://ffmpeg.org/&apos;&gt;FFMPEG&lt;/a&gt; wich is perfectly fine!
&lt;br&gt;
Sorry for this little inconvenience :)
&lt;br&gt;
&lt;br&gt;
&lt;center&gt;
&lt;b&gt;
IF YOU DO NOT HAVE A INTERNET CONNECT YOU CAN SIMPLY PUT FFMPEG AND FFPROBE
IN THE SAME FOLDER AS YOUR SCREENPLAY EXECUTABLE!
&lt;/b&gt;
&lt;br&gt;
&lt;br&gt;
This is usually:
&lt;br&gt; C:Program Files (x86)SteamsteamappscommonScreenPlay
&lt;br&gt;
if you installed ScreenPlay via Steam!
&lt;/center&gt;
&lt;br&gt;
&lt;br&gt;
&lt;b&gt;~ Kelteseth | Elias Steurer&lt;/b&gt;</source>
        <translation type="obsolete">&lt;b&gt;Почему мы беспокоим тебя этим?&lt;/b&gt;
&lt;br&gt;&lt;br&gt; Ну, это связано с &lt;b&gt;авторскими правами&lt;/b&gt; и множеством патентов&lt;/b&gt;.
Такие файлы, как .mp4 или .webm, являются контейнерами для видео и аудио. Каждый аудио
а видеофайл закодирован определенным кодеком. Они могут быть с открытым исходным кодом
и свободно использовать как &lt;a href=&apos;https://wikipedia.org/wiki/VP8&apos;&gt;VP8&lt;/a&gt; и более новый &lt;a href=&apos;https://wikipedia.org/wiki/VP9&apos;&gt;VP9&lt;/a&gt; (тот, который YouTube использует для их сети).
ms)но есть также некоторые проприетарные, такие как &lt;a href=&apos;https://wikipedia.org/wiki/H.264/MPEG-4_AVC&apos;&gt;h264&lt;/a&gt; и преемник &lt;a href=&apos;https://wikipedia.org/wiki/High_Efficiency_Video_Coding&apos;&gt;h265&lt;/a&gt;.
&lt;br&gt;
&lt;br&gt;


 Мы, как разработчики программного обеспечения, теперь должны иметь дело с такими вещами в области не имеют никакого опыта. Преследование, чтобы включить только бесплатные кодеки для контента было легко, но мы все еще должны предоставить способ для нашего пользователя, чтобы импортировать обои без хлопот. Мы не предоставляем &lt;a href=&apos;https://ffmpeg.org/&apos;&gt;FFMPEG&lt;/a&gt; для преобразования видео и аудио с ScreenPlay, потому что мы не имеем права. Мы позволяем пользователю загрузить &lt;a href=&apos;https://ffmpeg.org/&apos;&gt;FFMPEG&lt;/a&gt;, что совершенно нормально!
&lt;br&gt;
Приносим извинения за это небольшое неудобство :)
&lt;br&gt;
&lt;br&gt;
&lt;центр&gt;
&lt;b&gt;
ЕСЛИ У ВАС НЕТ ПОДКЛЮЧЕНИЯ К ИНТЕРНЕТУ, ВЫ МОЖЕТЕ ПРОСТО ПОСТАВИТЬ FFMPEG И FFPROBE.
В ТОЙ ЖЕ ПАПКЕ, ЧТО И ИСПОЛНЯЕМЫЙ ФАЙЛ ВАШЕГО СЦЕНАРИЯ!
&lt;/b&gt;
&lt;br&gt;
&lt;br&gt;
Обычно это так:
&lt;br&gt; C:Program Files (x86)SteamsteamappscommonScreenPlay
&lt;br&gt;
если вы установили ScreenPlay через Steam!
&lt;/центр&gt;
&lt;br&gt;
&lt;br&gt;
&lt;b&gt;~ Kelteseth | Elias Steurer&lt;/b&gt;</translation>
    </message>
    <message>
        <source>&lt;b&gt;Why do we bother you with this?&lt;/b&gt;
&lt;br&gt;&lt;br&gt; Well its because of &lt;b&gt;copyright&lt;/b&gt; and many many &lt;b&gt;patents&lt;/b&gt;.
Files like .mp4 or .webm are containers for video and audio. Every audio
and video file is encoded with a certain codec. These can be open sour
ceand free to use like &lt;a href=&apos;https://wikipedia.org/wiki/VP8&apos;&gt;VP8&lt;/a&gt; and the newer  &lt;a href=&apos;https://wikipedia.org/wiki/VP9&apos;&gt;VP9&lt;/a&gt; (the one YouTube uses for their web
ms)but there are also some proproatary ones like  &lt;a href=&apos;https://wikipedia.org/wiki/H.264/MPEG-4_AVC&apos;&gt;h264&lt;/a&gt; and the successor &lt;a href=&apos;https://wikipedia.org/wiki/High_Efficiency_Video_Coding&apos;&gt;h265&lt;/a&gt;.
&lt;br&gt;
&lt;br&gt;


 We as software developer now need to deal with stuff like this in a field we
are do not have any expertise in. The desicion to enable only free codecs for content was
an easy one but we still need to provide a way for our user to import wallpape
r without a hassle. We do not provide  &lt;a href=&apos;https://ffmpeg.org/&apos;&gt;FFMPEG&lt;/a&gt; f
or converting video and audio with ScreenPlay because we are not allowed to. We let the user download &lt;a href=&apos;https://ffmpeg.org/&apos;&gt;FFMPEG&lt;/a&gt; wich is perfectly fine!
&lt;br&gt;
Sorry for this little inconvenience :)
&lt;br&gt;
&lt;br&gt;
&lt;center&gt;
&lt;b&gt;
IF YOU DO NOT HAVE A INTERNET CONNECT YOU CAN SIMPLY PUT FFMPEG AND FFPROBE
IN THE SAME FOLDER AS YOUR SCREENPLAY EXECUTABLE!
&lt;/b&gt;
&lt;br&gt;
&lt;br&gt;
This is usually:
&lt;br&gt; C:Program Files (x86)SteamsteamappscommonScreenPlay
&lt;br&gt;
if you installed ScreenPlay via Steam!
&lt;/center&gt;
&lt;br&gt;
&lt;br&gt;
&lt;b&gt;~ Kelteseth | Elias Steurer&lt;/b&gt;</source>
        <translation type="obsolete">&lt;b&gt;Почему мы говорим это вам?&lt;/b&gt;
&lt;br&gt;&lt;br&gt; Что ж, это связано с &lt;b&gt;авторскими правами&lt;/b&gt; и множеством патентов&lt;/b&gt;.
Такие файлы, как .mp4 или .webm, являются контейнерами для видео и аудио. Каждый аудио
а видеофайл закодирован определенным кодеком. Они могут быть с открытым исходным кодом
и свободно использоваться, например &lt;a href=&apos;https://wikipedia.org/wiki/VP8&apos;&gt;VP8&lt;/a&gt; и более новый &lt;a href=&apos;https://wikipedia.org/wiki/VP9&apos;&gt;VP9&lt;/a&gt; (тот, который YouTube использует для их сети).
ms), но есть и некоторые проприетарные, такие как &lt;a href=&apos;https://wikipedia.org/wiki/H.264/MPEG-4_AVC&apos;&gt;h264&lt;/a&gt; и преемник &lt;a href=&apos;https://wikipedia.org/wiki/High_Efficiency_Video_Coding&apos;&gt;h265&lt;/a&gt;.
&lt;br&gt;
&lt;br&gt;


 Нам как разработчикам программного обеспечения теперь нужно иметь дело с такими вещами в области, в которой мы в этом деле
не имеем в этом деле никакого опыта. Было принято решение включить только бесплатные кодеки для контента.
Это легко, но нам все еще нужно предоставить способ для нашего пользователя импортировать обои без лишних хлопот. Мы не предоставляем &lt;a href=&apos;https://ffmpeg.org/&apos;&gt;FFMPEG&lt;/a&gt;
для преобразования видео и аудио с помощью ScreenPlay, потому что мы не имеем права. Мы разрешили пользователю загрузить &lt;a href=&apos;https://ffmpeg.org/&apos;&gt;FFMPEG&lt;/a&gt;, что совершенно нормально!
&lt;br&gt;
Приносим прощения за это небольшое неудобство :)
&lt;br&gt;
&lt;br&gt;
&lt;center&gt;
&lt;b&gt;
ЕСЛИ У ВАС НЕТ ПОДКЛЮЧЕНИЯ К ИНТЕРНЕТУ, ВЫ МОЖЕТЕ САМОСТОЯТЕЛЬНО УСТАНОВИТЬ FFMPEG И FFPROBE.
ОНИ НАХОДЯТСЯ В ТОЙ ЖЕ ПАПКЕ, ГДЕ И EXE-ФАЙЛ SCREENPLAY!
&lt;/b&gt;
&lt;br&gt;
&lt;br&gt;
Обычно это так:
&lt;br&gt; C:Program Files (x86)SteamsteamappscommonScreenPlay
&lt;br&gt;
если вы установили ScreenPlay через Steam!
&lt;/center&gt;
&lt;br&gt;
&lt;br&gt;
&lt;b&gt;~ Kelteseth | Elias Steurer&lt;/b&gt;</translation>
    </message>
    <message>
        <source>Not now!</source>
        <translation type="obsolete">Не сейчас!</translation>
    </message>
    <message>
        <source>You can now start creating content!</source>
        <translation type="obsolete">Теперь вы можете приступить к созданию контента!</translation>
    </message>
    <message>
        <source>Start!</source>
        <translation type="obsolete">Начать!</translation>
    </message>
</context>
<context>
    <name>Footer</name>
    <message>
        <location filename="../qml/Create/Footer.qml" line="23"/>
        <source>QML Quickstart Guide</source>
        <translation type="unfinished">Краткое руководство по QML</translation>
    </message>
    <message>
        <location filename="../qml/Create/Footer.qml" line="34"/>
        <source>Documentation</source>
        <translation type="unfinished">Документация</translation>
    </message>
    <message>
        <location filename="../qml/Create/Footer.qml" line="45"/>
        <source>Forums</source>
        <translation type="unfinished">Форум</translation>
    </message>
    <message>
        <location filename="../qml/Create/Footer.qml" line="56"/>
        <source>Workshop</source>
        <translation type="unfinished">Мастерская</translation>
    </message>
</context>
<context>
    <name>Headline</name>
    <message>
        <location filename="../qml/Common/Headline.qml" line="16"/>
        <source>Headline</source>
        <translation type="unfinished">Заголовок</translation>
    </message>
</context>
<context>
    <name>ImageSelector</name>
    <message>
        <location filename="../qml/Common/ImageSelector.qml" line="142"/>
        <source>Clear</source>
        <translation type="unfinished">Очистить</translation>
    </message>
    <message>
        <location filename="../qml/Common/ImageSelector.qml" line="160"/>
        <source>Select Preview Image</source>
        <translation type="unfinished">Выберите Предварительный просмотр изображения</translation>
    </message>
</context>
<context>
    <name>ImportContent</name>
    <message>
        <location filename="../qml/Create/ImportContent.qml" line="22"/>
        <source>Import Content</source>
        <translation type="unfinished">Импорт содержимого</translation>
    </message>
    <message>
        <location filename="../qml/Create/ImportContent.qml" line="75"/>
        <source>Import video</source>
        <translation type="unfinished">Импорт видео</translation>
    </message>
    <message>
        <source>FFMPEG Needed for import</source>
        <translation type="obsolete">FFMPEG необходим для импорта</translation>
    </message>
    <message>
        <source>Import ThreeJs Scene</source>
        <translation type="obsolete">Импортировать сцены ThreeJs</translation>
    </message>
    <message>
        <location filename="../qml/Create/ImportContent.qml" line="161"/>
        <source>Upload Exsisting Project to Steam</source>
        <translation type="unfinished">Загрузить существующий проект в Steam</translation>
    </message>
</context>
<context>
    <name>Installed</name>
    <message>
        <location filename="../qml/Installed/Installed.qml" line="99"/>
        <source>Refreshing!</source>
        <translation type="unfinished">Обновляю!</translation>
    </message>
    <message>
        <location filename="../qml/Installed/Installed.qml" line="102"/>
        <location filename="../qml/Installed/Installed.qml" line="124"/>
        <source>Pull to refresh!</source>
        <translation type="unfinished">Тяни, чтобы обновить!</translation>
    </message>
    <message>
        <location filename="../qml/Installed/Installed.qml" line="141"/>
        <source>Get more Wallpaper &amp; Widgets via the Steam workshop!</source>
        <translation type="unfinished">Получите больше обоев и виджетов через мастерскую Steam!</translation>
    </message>
    <message>
        <location filename="../qml/Installed/Installed.qml" line="217"/>
        <source>Open containing folder</source>
        <translation type="unfinished">Открыть содержащую папку</translation>
    </message>
    <message>
        <location filename="../qml/Installed/Installed.qml" line="225"/>
        <source>Deinstall Item</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/Installed/Installed.qml" line="234"/>
        <source>Open workshop Page</source>
        <translation type="unfinished">Открыть страницу мастерской</translation>
    </message>
    <message>
        <location filename="../qml/Installed/Installed.qml" line="245"/>
        <source>Are you sure you want to delete this item?</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>All</source>
        <translation type="obsolete">Все</translation>
    </message>
    <message>
        <source>Videos</source>
        <translation type="obsolete">Видео</translation>
    </message>
    <message>
        <source>Scenes</source>
        <translation type="obsolete">Сцены</translation>
    </message>
    <message>
        <source>Widgets</source>
        <translation type="obsolete">Виджеты</translation>
    </message>
    <message>
        <location filename="../qml/Installed/Installed.qml" line="329"/>
        <source></source>
        <translation></translation>
    </message>
    <message>
        <source>Search for Wallpaper &amp; Widgets</source>
        <translation type="obsolete">Поиск обоев и виджетов</translation>
    </message>
</context>
<context>
    <name>InstalledWelcomeScreen</name>
    <message>
        <location filename="../qml/Installed/InstalledWelcomeScreen.qml" line="54"/>
        <source>Get free Widgets and Wallpaper via the Steam Workshop</source>
        <translation type="unfinished">Получите бесплатные виджеты и обои через Steam</translation>
    </message>
    <message>
        <location filename="../qml/Installed/InstalledWelcomeScreen.qml" line="84"/>
        <source>Browse the Steam Workshop</source>
        <translation type="unfinished">Просмотреть мастерскую Steam</translation>
    </message>
</context>
<context>
    <name>Monitors</name>
    <message>
        <location filename="../qml/Monitors/Monitors.qml" line="67"/>
        <source>Wallpaper Configuration</source>
        <translation type="unfinished">Настройка обоев</translation>
    </message>
    <message>
        <location filename="../qml/Monitors/Monitors.qml" line="132"/>
        <source>Remove selected</source>
        <translation type="unfinished">Удалить выбранные</translation>
    </message>
    <message>
        <location filename="../qml/Monitors/Monitors.qml" line="144"/>
        <location filename="../qml/Monitors/Monitors.qml" line="158"/>
        <source>Remove </source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/Monitors/Monitors.qml" line="145"/>
        <source>Wallpapers</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/Monitors/Monitors.qml" line="159"/>
        <source>Widgets</source>
        <translation type="unfinished">Виджеты</translation>
    </message>
    <message>
        <source>Remove all Wallpapers</source>
        <translation type="obsolete">Удалить все обои</translation>
    </message>
    <message>
        <source>Remove all Widgets</source>
        <translation type="obsolete">Удалить все виджеты</translation>
    </message>
</context>
<context>
    <name>MonitorsProjectSettingItem</name>
    <message>
        <location filename="../qml/Monitors/MonitorsProjectSettingItem.qml" line="132"/>
        <source>Set color</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/Monitors/MonitorsProjectSettingItem.qml" line="155"/>
        <source>Please choose a color</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>Navigation</name>
    <message>
        <location filename="../qml/Installed/Navigation.qml" line="57"/>
        <source>All</source>
        <translation type="unfinished">Все</translation>
    </message>
    <message>
        <location filename="../qml/Installed/Navigation.qml" line="73"/>
        <source>Scenes</source>
        <translation type="unfinished">Сцены</translation>
    </message>
    <message>
        <location filename="../qml/Installed/Navigation.qml" line="89"/>
        <source>Videos</source>
        <translation type="unfinished">Видео</translation>
    </message>
    <message>
        <location filename="../qml/Installed/Navigation.qml" line="105"/>
        <source>Widgets</source>
        <translation type="unfinished">Виджеты</translation>
    </message>
    <message>
        <location filename="../qml/Workshop/Navigation.qml" line="46"/>
        <source> Subscribed items: </source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/Workshop/Navigation.qml" line="87"/>
        <source>Upload to the Steam Workshop</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>NavigationWallpaperConfiguration</name>
    <message>
        <location filename="../qml/Navigation/NavigationWallpaperConfiguration.qml" line="70"/>
        <source>Configurate active Wallpaper or Widgets</source>
        <translation type="unfinished">Настройка активных обоев или виджетов</translation>
    </message>
    <message>
        <location filename="../qml/Navigation/NavigationWallpaperConfiguration.qml" line="72"/>
        <source>No active Wallpaper or Widgets</source>
        <translation type="unfinished">Нет активных обоев или виджетов</translation>
    </message>
</context>
<context>
    <name>PopupOffline</name>
    <message>
        <location filename="../qml/Workshop/PopupOffline.qml" line="28"/>
        <source>You need to run Steam for this :)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/Workshop/PopupOffline.qml" line="37"/>
        <source>Back</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>PopupSteamWorkshopAgreement</name>
    <message>
        <location filename="../qml/Workshop/upload/PopupSteamWorkshopAgreement.qml" line="22"/>
        <source>Abort Upload.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/Workshop/upload/PopupSteamWorkshopAgreement.qml" line="30"/>
        <source>I Agree to the Steam Workshop Agreement</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>SaveNotification</name>
    <message>
        <location filename="../qml/Monitors/SaveNotification.qml" line="40"/>
        <source>Profile saved successfully!</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>ScreenPlayItem</name>
    <message>
        <source>Open containing folder</source>
        <translation type="obsolete">Открыть содержащую папку</translation>
    </message>
    <message>
        <source>Open workshop Page</source>
        <translation type="obsolete">Открыть страницу мастерской</translation>
    </message>
</context>
<context>
    <name>Search</name>
    <message>
        <location filename="../qml/Common/Search.qml" line="45"/>
        <source>Search for Wallpaper &amp; Widgets</source>
        <translation type="unfinished">Поиск обоев и виджетов</translation>
    </message>
</context>
<context>
    <name>Settings</name>
    <message>
        <location filename="../qml/Settings/Settings.qml" line="59"/>
        <source>General</source>
        <translation type="unfinished">Общие сведения</translation>
    </message>
    <message>
        <location filename="../qml/Settings/Settings.qml" line="74"/>
        <source>Autostart</source>
        <translation type="unfinished">Автозапуск</translation>
    </message>
    <message>
        <location filename="../qml/Settings/Settings.qml" line="75"/>
        <source>ScreenPlay will start with Windows and will setup your Desktop every time for you.</source>
        <translation type="unfinished">ScreenPlay запустится с Windows и каждый раз будет настраивать ваш рабочий стол для вас.</translation>
    </message>
    <message>
        <location filename="../qml/Settings/Settings.qml" line="83"/>
        <source>High priority Autostart</source>
        <translation type="unfinished">Высокий приоритет автостарта</translation>
    </message>
    <message>
        <location filename="../qml/Settings/Settings.qml" line="86"/>
        <source>This options grants ScreenPlay a higher autostart priority than other apps.</source>
        <translation type="unfinished">Эта опция предоставляет ScreenPlay более высокий приоритет автозапуска по сравнению с другими приложениями.</translation>
    </message>
    <message>
        <location filename="../qml/Settings/Settings.qml" line="95"/>
        <source>Send anonymous crash reports and statistics</source>
        <translation type="unfinished">Отправлять анонимные сообщения о падениях и статистику</translation>
    </message>
    <message>
        <location filename="../qml/Settings/Settings.qml" line="96"/>
        <source>Help us make ScreenPlay faster and more stable. All collected data is purely anonymous and only used for development purposes!</source>
        <translation type="unfinished">Помогите нам сделать ScreenPlay более быстрым и стабильным. Все собранные данные являются абсолютно анонимными и используются только в целях разработки!</translation>
    </message>
    <message>
        <location filename="../qml/Settings/Settings.qml" line="105"/>
        <source>Set save location</source>
        <translation type="unfinished">Установить расположение для сохранения</translation>
    </message>
    <message>
        <location filename="../qml/Settings/Settings.qml" line="110"/>
        <source>Your storage path is empty!</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/Settings/Settings.qml" line="116"/>
        <source>Set location</source>
        <translation type="unfinished">Выбрать расположение</translation>
    </message>
    <message>
        <location filename="../qml/Settings/Settings.qml" line="131"/>
        <source>Important: Changing this directory has no effect on the workshop download path. ScreenPlay only supports having one content folder!</source>
        <translation type="unfinished">Важно: Изменение этой директории не влияет на путь загрузки для мастерской. ScreenPlay поддерживает только одну папку с содержимым!</translation>
    </message>
    <message>
        <location filename="../qml/Settings/Settings.qml" line="149"/>
        <source>Language</source>
        <translation type="unfinished">Язык</translation>
    </message>
    <message>
        <location filename="../qml/Settings/Settings.qml" line="150"/>
        <source>Set the ScreenPlay UI Language</source>
        <translation type="unfinished">Установите язык интерфейса ScreenPlay</translation>
    </message>
    <message>
        <location filename="../qml/Settings/Settings.qml" line="165"/>
        <source>English</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/Settings/Settings.qml" line="168"/>
        <source>German</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/Settings/Settings.qml" line="174"/>
        <source>Russian</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/Settings/Settings.qml" line="177"/>
        <source>French</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/Settings/Settings.qml" line="180"/>
        <source>Spanish</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/Settings/Settings.qml" line="183"/>
        <source>Korean</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/Settings/Settings.qml" line="186"/>
        <source>Vietnamese</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/Settings/Settings.qml" line="194"/>
        <source>Theme</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/Settings/Settings.qml" line="195"/>
        <source>Switch dark/light theme</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/Settings/Settings.qml" line="209"/>
        <source>System Default</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/Settings/Settings.qml" line="212"/>
        <source>Dark</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/Settings/Settings.qml" line="215"/>
        <source>Light</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/Settings/Settings.qml" line="225"/>
        <source>Performance</source>
        <translation type="unfinished">Производительность</translation>
    </message>
    <message>
        <location filename="../qml/Settings/Settings.qml" line="240"/>
        <source>Pause wallpaper video rendering while another app is in the foreground</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/Settings/Settings.qml" line="241"/>
        <source>We disable the video rendering (not the audio!) for the best performance. If you have problem you can disable this behaviour here. Wallpaper restart required!</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Pause wallpaper while ingame</source>
        <translation type="obsolete">Остановить обои во время игры</translation>
    </message>
    <message>
        <source>To maximise your framerates ingame, you can enable this setting to pause all active wallpapers!</source>
        <translation type="obsolete">Чтобы максимизировать количество кадров в игре, вы можете включить эту настройку, чтобы поставить на паузу все активные обои!</translation>
    </message>
    <message>
        <location filename="../qml/Settings/Settings.qml" line="171"/>
        <source>Chinese - Simplified</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/Settings/Settings.qml" line="251"/>
        <source>Default Fill Mode</source>
        <translation type="unfinished">Режим заполнения по умолчанию</translation>
    </message>
    <message>
        <location filename="../qml/Settings/Settings.qml" line="252"/>
        <source>Set this property to define how the video is scaled to fit the target area.</source>
        <translation type="unfinished">Включите эту настройку, чтобы определить, как видео масштабируется в соответствии с целевой областью.</translation>
    </message>
    <message>
        <location filename="../qml/Settings/Settings.qml" line="264"/>
        <source>Stretch</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/Settings/Settings.qml" line="267"/>
        <source>Fill</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/Settings/Settings.qml" line="270"/>
        <source>Contain</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/Settings/Settings.qml" line="273"/>
        <source>Cover</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/Settings/Settings.qml" line="276"/>
        <source>Scale-Down</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/Settings/Settings.qml" line="286"/>
        <source>About</source>
        <translation type="unfinished">О сайте</translation>
    </message>
    <message>
        <location filename="../qml/Settings/Settings.qml" line="314"/>
        <source>Thank you for using ScreenPlay</source>
        <translation type="unfinished">Спасибо за использование ScreenPlay</translation>
    </message>
    <message>
        <location filename="../qml/Settings/Settings.qml" line="329"/>
        <source>Hi, I&apos;m Elias Steurer also known as Kelteseth and I&apos;m the developer of ScreenPlay. Thank you for using my software. You can follow me to receive updates about ScreenPlay here:</source>
        <translation type="unfinished">Привет, я Elias Steurer, также известный как Kelteseth, и я разработчик ScreenPlay. Спасибо за использование моего программного обеспечения. Вы можете подписаться на меня, чтобы получать обновления о ScreenPlay:</translation>
    </message>
    <message>
        <location filename="../qml/Settings/Settings.qml" line="418"/>
        <source>Version</source>
        <translation type="unfinished">Версия</translation>
    </message>
    <message>
        <location filename="../qml/Settings/Settings.qml" line="419"/>
        <source>ScreenPlay Build Version </source>
        <translation type="unfinished">Версия сборки ScreenPlay </translation>
    </message>
    <message>
        <location filename="../qml/Settings/Settings.qml" line="421"/>
        <source>Open Changelog</source>
        <translation type="unfinished">Открыть журнал изменений</translation>
    </message>
    <message>
        <location filename="../qml/Settings/Settings.qml" line="428"/>
        <source>Third Party Software</source>
        <translation type="unfinished">Стороннее ПО</translation>
    </message>
    <message>
        <location filename="../qml/Settings/Settings.qml" line="429"/>
        <source>ScreenPlay would not be possible without the work of others. A big thank you to: </source>
        <translation type="unfinished">ScreenPlay был бы невозможен без работы других. Большое спасибо: </translation>
    </message>
    <message>
        <location filename="../qml/Settings/Settings.qml" line="430"/>
        <source>Licenses</source>
        <translation type="unfinished">Лицензии</translation>
    </message>
    <message>
        <location filename="../qml/Settings/Settings.qml" line="448"/>
        <location filename="../qml/Settings/Settings.qml" line="450"/>
        <source>Debug Messages</source>
        <translation type="unfinished">Отладочные сообщения</translation>
    </message>
    <message>
        <location filename="../qml/Settings/Settings.qml" line="449"/>
        <source>If your ScreenPlay missbehaves this is a good way to look for answers. This shows all logs and warning during runtime.</source>
        <translation type="unfinished">Если ваш ScreenPlay плохо себя ведет, это хороший способ поиска ответов. Он показывает все журналы и предупреждения во время работы.</translation>
    </message>
    <message>
        <location filename="../qml/Settings/Settings.qml" line="461"/>
        <source>Data Protection</source>
        <translation type="unfinished">Защита данных</translation>
    </message>
    <message>
        <location filename="../qml/Settings/Settings.qml" line="462"/>
        <source>We use you data very carefully to improve ScreenPlay. We do not sell or share this (anonymous) information with others!</source>
        <translation type="unfinished">Мы используем ваши данные очень тщательно, чтобы улучшить ScreenPlay. Мы не продаем и не делимся этой (анонимной) информацией с другими!</translation>
    </message>
    <message>
        <location filename="../qml/Settings/Settings.qml" line="463"/>
        <source>Privacy</source>
        <translation type="unfinished">Конфиденциальность</translation>
    </message>
</context>
<context>
    <name>SettingsExpander</name>
    <message>
        <location filename="../qml/Settings/SettingsExpander.qml" line="51"/>
        <source>Copy text to clipboard</source>
        <translation type="unfinished">Скопировать текст в буфер обмена</translation>
    </message>
</context>
<context>
    <name>Sidebar</name>
    <message>
        <location filename="../qml/Installed/Sidebar.qml" line="99"/>
        <source>Set Widget</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/Installed/Sidebar.qml" line="208"/>
        <source>Headline</source>
        <translation type="unfinished">Заголовок</translation>
    </message>
    <message>
        <location filename="../qml/Installed/Sidebar.qml" line="259"/>
        <source>Select a Monitor to display the content</source>
        <translation type="unfinished">Выберите Монитор для отображения содержимого</translation>
    </message>
    <message>
        <location filename="../qml/Installed/Sidebar.qml" line="294"/>
        <source>Set Volume</source>
        <translation type="unfinished">Установить громкость звука</translation>
    </message>
    <message>
        <location filename="../qml/Installed/Sidebar.qml" line="304"/>
        <source>Fill Mode</source>
        <translation type="unfinished">Режим заполнения</translation>
    </message>
    <message>
        <location filename="../qml/Installed/Sidebar.qml" line="327"/>
        <source>Stretch</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/Installed/Sidebar.qml" line="330"/>
        <source>Fill</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/Installed/Sidebar.qml" line="333"/>
        <source>Contain</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/Installed/Sidebar.qml" line="336"/>
        <source>Cover</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/Installed/Sidebar.qml" line="339"/>
        <source>Scale-Down</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Set wallpaper</source>
        <translation type="obsolete">Установить обои</translation>
    </message>
    <message>
        <location filename="../qml/Installed/Sidebar.qml" line="96"/>
        <source>Set Wallpaper</source>
        <translation type="unfinished">Установить обои</translation>
    </message>
    <message>
        <source>Create Widget</source>
        <translation type="vanished">Создать виджет</translation>
    </message>
    <message>
        <source>Create Wallpaper</source>
        <translation type="obsolete">Создать обои</translation>
    </message>
    <message>
        <location filename="../qml/Workshop/Sidebar.qml" line="91"/>
        <source>Project size: </source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/Workshop/Sidebar.qml" line="91"/>
        <source> MB</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/Workshop/Sidebar.qml" line="97"/>
        <source>No description...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/Workshop/Sidebar.qml" line="239"/>
        <source>Click here if you like the content</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/Workshop/Sidebar.qml" line="253"/>
        <source>Click here if you do not like the content</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/Workshop/Sidebar.qml" line="288"/>
        <source>Tags: </source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/Workshop/Sidebar.qml" line="313"/>
        <source>Subscribtions: </source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/Workshop/Sidebar.qml" line="319"/>
        <source>Open In Steam</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/Workshop/Sidebar.qml" line="362"/>
        <source>Subscribed!</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/Workshop/Sidebar.qml" line="362"/>
        <source>Subscribe</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>TagSelector</name>
    <message>
        <location filename="../qml/Common/TagSelector.qml" line="12"/>
        <source>Save</source>
        <translation type="unfinished">Сохранить</translation>
    </message>
    <message>
        <location filename="../qml/Common/TagSelector.qml" line="15"/>
        <source>Add tag</source>
        <translation>Добавить тэг</translation>
    </message>
    <message>
        <location filename="../qml/Common/TagSelector.qml" line="109"/>
        <source>Cancel</source>
        <translation type="unfinished">Отменить</translation>
    </message>
    <message>
        <location filename="../qml/Common/TagSelector.qml" line="128"/>
        <source>Add Tag</source>
        <translation type="unfinished">Добавить тэг</translation>
    </message>
</context>
<context>
    <name>TrayIcon</name>
    <message>
        <location filename="../qml/Common/TrayIcon.qml" line="9"/>
        <source>ScreenPlay - Double click to change you settings.</source>
        <translation type="unfinished">ScreenPlay - Двойной щелчок для изменения настроек.</translation>
    </message>
    <message>
        <location filename="../qml/Common/TrayIcon.qml" line="28"/>
        <source>Open ScreenPlay</source>
        <translation type="unfinished">Открыть ScreenPlay</translation>
    </message>
    <message>
        <location filename="../qml/Common/TrayIcon.qml" line="36"/>
        <location filename="../qml/Common/TrayIcon.qml" line="40"/>
        <source>Mute all</source>
        <translation type="unfinished">Отключить все звуки</translation>
    </message>
    <message>
        <location filename="../qml/Common/TrayIcon.qml" line="45"/>
        <source>Unmute all</source>
        <translation type="unfinished">Отключить все</translation>
    </message>
    <message>
        <location filename="../qml/Common/TrayIcon.qml" line="54"/>
        <location filename="../qml/Common/TrayIcon.qml" line="58"/>
        <source>Pause all</source>
        <translation type="unfinished">Остановить все</translation>
    </message>
    <message>
        <location filename="../qml/Common/TrayIcon.qml" line="63"/>
        <source>Play all</source>
        <translation type="unfinished">Играть всё</translation>
    </message>
    <message>
        <location filename="../qml/Common/TrayIcon.qml" line="70"/>
        <source>Quit</source>
        <translation type="unfinished">Выход</translation>
    </message>
</context>
<context>
    <name>UploadProject</name>
    <message>
        <location filename="../qml/Workshop/upload/UploadProject.qml" line="63"/>
        <source>Upload Wallpaper/Widgets to Steam</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/Workshop/upload/UploadProject.qml" line="137"/>
        <source>Abort</source>
        <translation type="unfinished">Отменить</translation>
    </message>
    <message>
        <location filename="../qml/Workshop/upload/UploadProject.qml" line="152"/>
        <source>Upload Projects</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/Workshop/upload/UploadProject.qml" line="212"/>
        <source>Finish</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>UploadProjectBigItem</name>
    <message>
        <location filename="../qml/Workshop/upload/UploadProjectBigItem.qml" line="114"/>
        <source>Type: </source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/Workshop/upload/UploadProjectBigItem.qml" line="121"/>
        <source>Open Folder</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/Workshop/upload/UploadProjectBigItem.qml" line="141"/>
        <source>Invalid Project!</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>UploadProjectItem</name>
    <message>
        <location filename="../qml/Workshop/upload/UploadProjectItem.qml" line="43"/>
        <source>Fail</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/Workshop/upload/UploadProjectItem.qml" line="46"/>
        <source>No Connection</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/Workshop/upload/UploadProjectItem.qml" line="49"/>
        <source>Invalid Password</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/Workshop/upload/UploadProjectItem.qml" line="52"/>
        <source>Logged In Elsewhere</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/Workshop/upload/UploadProjectItem.qml" line="55"/>
        <source>Invalid Protocol Version</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/Workshop/upload/UploadProjectItem.qml" line="58"/>
        <source>Invalid Param</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/Workshop/upload/UploadProjectItem.qml" line="61"/>
        <source>File Not Found</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/Workshop/upload/UploadProjectItem.qml" line="64"/>
        <source>Busy</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/Workshop/upload/UploadProjectItem.qml" line="67"/>
        <source>Invalid State</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/Workshop/upload/UploadProjectItem.qml" line="70"/>
        <source>Invalid Name</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/Workshop/upload/UploadProjectItem.qml" line="73"/>
        <source>Invalid Email</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/Workshop/upload/UploadProjectItem.qml" line="76"/>
        <source>Duplicate Name</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/Workshop/upload/UploadProjectItem.qml" line="79"/>
        <source>Access Denied</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/Workshop/upload/UploadProjectItem.qml" line="82"/>
        <source>Timeout</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/Workshop/upload/UploadProjectItem.qml" line="85"/>
        <source>Banned</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/Workshop/upload/UploadProjectItem.qml" line="88"/>
        <source>Account Not Found</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/Workshop/upload/UploadProjectItem.qml" line="91"/>
        <source>Invalid SteamID</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/Workshop/upload/UploadProjectItem.qml" line="94"/>
        <source>Service Unavailable</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/Workshop/upload/UploadProjectItem.qml" line="97"/>
        <source>Not Logged On</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/Workshop/upload/UploadProjectItem.qml" line="100"/>
        <source>Pending</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/Workshop/upload/UploadProjectItem.qml" line="103"/>
        <source>Encryption Failure</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/Workshop/upload/UploadProjectItem.qml" line="106"/>
        <source>Insufficient Privilege</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/Workshop/upload/UploadProjectItem.qml" line="109"/>
        <source>Limit Exceeded</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/Workshop/upload/UploadProjectItem.qml" line="112"/>
        <source>Revoked</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/Workshop/upload/UploadProjectItem.qml" line="115"/>
        <source>Expired</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/Workshop/upload/UploadProjectItem.qml" line="118"/>
        <source>Already Redeemed</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/Workshop/upload/UploadProjectItem.qml" line="121"/>
        <source>Duplicate Request</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/Workshop/upload/UploadProjectItem.qml" line="124"/>
        <source>Already Owned</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/Workshop/upload/UploadProjectItem.qml" line="127"/>
        <source>IP Not Found</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/Workshop/upload/UploadProjectItem.qml" line="130"/>
        <source>Persist Failed</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/Workshop/upload/UploadProjectItem.qml" line="133"/>
        <source>Locking Failed</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/Workshop/upload/UploadProjectItem.qml" line="136"/>
        <source>Logon Session Replaced</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/Workshop/upload/UploadProjectItem.qml" line="139"/>
        <source>Connect Failed</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/Workshop/upload/UploadProjectItem.qml" line="142"/>
        <source>Handshake Failed</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/Workshop/upload/UploadProjectItem.qml" line="145"/>
        <source>IO Failure</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/Workshop/upload/UploadProjectItem.qml" line="148"/>
        <source>Remote Disconnect</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/Workshop/upload/UploadProjectItem.qml" line="151"/>
        <source>Shopping Cart Not Found</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/Workshop/upload/UploadProjectItem.qml" line="154"/>
        <source>Blocked</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/Workshop/upload/UploadProjectItem.qml" line="157"/>
        <source>Ignored</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/Workshop/upload/UploadProjectItem.qml" line="160"/>
        <source>No Match</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/Workshop/upload/UploadProjectItem.qml" line="163"/>
        <source>Account Disabled</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/Workshop/upload/UploadProjectItem.qml" line="166"/>
        <source>Service ReadOnly</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/Workshop/upload/UploadProjectItem.qml" line="169"/>
        <source>Account Not Featured</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/Workshop/upload/UploadProjectItem.qml" line="172"/>
        <source>Administrator OK</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/Workshop/upload/UploadProjectItem.qml" line="175"/>
        <source>Content Version</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/Workshop/upload/UploadProjectItem.qml" line="178"/>
        <source>Try Another CM</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/Workshop/upload/UploadProjectItem.qml" line="181"/>
        <source>Password Required T oKick Session</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/Workshop/upload/UploadProjectItem.qml" line="184"/>
        <source>Already Logged In Elsewhere</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/Workshop/upload/UploadProjectItem.qml" line="187"/>
        <source>Suspended</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/Workshop/upload/UploadProjectItem.qml" line="190"/>
        <source>Cancelled</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/Workshop/upload/UploadProjectItem.qml" line="193"/>
        <source>Data Corruption</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/Workshop/upload/UploadProjectItem.qml" line="196"/>
        <source>Disk Full</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/Workshop/upload/UploadProjectItem.qml" line="199"/>
        <source>Remote Call Failed</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/Workshop/upload/UploadProjectItem.qml" line="202"/>
        <source>Password Unset</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/Workshop/upload/UploadProjectItem.qml" line="205"/>
        <source>External Account Unlinked</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/Workshop/upload/UploadProjectItem.qml" line="208"/>
        <source>PSN Ticket Invalid</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/Workshop/upload/UploadProjectItem.qml" line="211"/>
        <source>External Account Already Linked</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/Workshop/upload/UploadProjectItem.qml" line="214"/>
        <source>Remote File Conflict</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/Workshop/upload/UploadProjectItem.qml" line="217"/>
        <source>Illegal Password</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/Workshop/upload/UploadProjectItem.qml" line="220"/>
        <source>Same As Previous Value</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/Workshop/upload/UploadProjectItem.qml" line="223"/>
        <source>Account Logon Denied</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/Workshop/upload/UploadProjectItem.qml" line="226"/>
        <source>Cannot Use Old Password</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/Workshop/upload/UploadProjectItem.qml" line="229"/>
        <source>Invalid Login AuthCode</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/Workshop/upload/UploadProjectItem.qml" line="232"/>
        <source>Account Logon Denied No Mail</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/Workshop/upload/UploadProjectItem.qml" line="235"/>
        <source>Hardware Not Capable Of IPT</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/Workshop/upload/UploadProjectItem.qml" line="238"/>
        <source>IPT Init Error</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/Workshop/upload/UploadProjectItem.qml" line="241"/>
        <source>Parental Control Restricted</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/Workshop/upload/UploadProjectItem.qml" line="244"/>
        <source>Facebook Query Error</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/Workshop/upload/UploadProjectItem.qml" line="247"/>
        <source>Expired Login Auth Code</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/Workshop/upload/UploadProjectItem.qml" line="250"/>
        <source>IP Login Restriction Failed</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/Workshop/upload/UploadProjectItem.qml" line="253"/>
        <source>Account Locked Down</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/Workshop/upload/UploadProjectItem.qml" line="256"/>
        <source>Account Logon Denied Verified Email Required</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/Workshop/upload/UploadProjectItem.qml" line="259"/>
        <source>No MatchingURL</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/Workshop/upload/UploadProjectItem.qml" line="262"/>
        <source>Bad Response</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/Workshop/upload/UploadProjectItem.qml" line="265"/>
        <source>Require Password ReEntry</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/Workshop/upload/UploadProjectItem.qml" line="268"/>
        <source>Value Out Of Range</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/Workshop/upload/UploadProjectItem.qml" line="271"/>
        <source>Unexpecte Error</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/Workshop/upload/UploadProjectItem.qml" line="274"/>
        <source>Disabled</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/Workshop/upload/UploadProjectItem.qml" line="277"/>
        <source>Invalid CEG Submission</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/Workshop/upload/UploadProjectItem.qml" line="280"/>
        <source>Restricted Device</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/Workshop/upload/UploadProjectItem.qml" line="283"/>
        <source>Region Locked</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/Workshop/upload/UploadProjectItem.qml" line="286"/>
        <source>Rate Limit Exceeded</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/Workshop/upload/UploadProjectItem.qml" line="289"/>
        <source>Account Login Denied Need Two Factor</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/Workshop/upload/UploadProjectItem.qml" line="292"/>
        <source>Item Deleted</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/Workshop/upload/UploadProjectItem.qml" line="295"/>
        <source>Account Login Denied Throttle</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/Workshop/upload/UploadProjectItem.qml" line="298"/>
        <source>Two Factor Code Mismatch</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/Workshop/upload/UploadProjectItem.qml" line="301"/>
        <source>Two Factor Activation Code Mismatch</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/Workshop/upload/UploadProjectItem.qml" line="304"/>
        <source>Account Associated To Multiple Partners</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/Workshop/upload/UploadProjectItem.qml" line="307"/>
        <source>Not Modified</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/Workshop/upload/UploadProjectItem.qml" line="310"/>
        <source>No Mobile Device</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/Workshop/upload/UploadProjectItem.qml" line="313"/>
        <source>Time Not Synced</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/Workshop/upload/UploadProjectItem.qml" line="316"/>
        <source>Sms Code Failed</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/Workshop/upload/UploadProjectItem.qml" line="319"/>
        <source>Account Limit Exceeded</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/Workshop/upload/UploadProjectItem.qml" line="322"/>
        <source>Account Activity Limit Exceeded</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/Workshop/upload/UploadProjectItem.qml" line="325"/>
        <source>Phone Activity Limit Exceeded</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/Workshop/upload/UploadProjectItem.qml" line="328"/>
        <source>Refund To Wallet</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/Workshop/upload/UploadProjectItem.qml" line="331"/>
        <source>Email Send Failure</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/Workshop/upload/UploadProjectItem.qml" line="334"/>
        <source>Not Settled</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/Workshop/upload/UploadProjectItem.qml" line="337"/>
        <source>Need Captcha</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/Workshop/upload/UploadProjectItem.qml" line="340"/>
        <source>GSLT Denied</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/Workshop/upload/UploadProjectItem.qml" line="343"/>
        <source>GS Owner Denied</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/Workshop/upload/UploadProjectItem.qml" line="346"/>
        <source>Invalid Item Type</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/Workshop/upload/UploadProjectItem.qml" line="349"/>
        <source>IP Banned</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/Workshop/upload/UploadProjectItem.qml" line="352"/>
        <source>GSLT Expired</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/Workshop/upload/UploadProjectItem.qml" line="355"/>
        <source>Insufficient Funds</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/Workshop/upload/UploadProjectItem.qml" line="358"/>
        <source>Too Many Pending</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/Workshop/upload/UploadProjectItem.qml" line="361"/>
        <source>No Site Licenses Found</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/Workshop/upload/UploadProjectItem.qml" line="364"/>
        <source>WG Network Send Exceeded</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/Workshop/upload/UploadProjectItem.qml" line="367"/>
        <source>Account Not Friends</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/Workshop/upload/UploadProjectItem.qml" line="370"/>
        <source>Limited User Account</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/Workshop/upload/UploadProjectItem.qml" line="373"/>
        <source>Cant Remove Item</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/Workshop/upload/UploadProjectItem.qml" line="376"/>
        <source>Account Deleted</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/Workshop/upload/UploadProjectItem.qml" line="379"/>
        <source>Existing User Cancelled License</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/Workshop/upload/UploadProjectItem.qml" line="382"/>
        <source>Community Cooldown</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/Workshop/upload/UploadProjectItem.qml" line="446"/>
        <source>Status:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/Workshop/upload/UploadProjectItem.qml" line="463"/>
        <source>Upload Progress: </source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>Workshop</name>
    <message>
        <location filename="../qml/Workshop/Workshop.qml" line="133"/>
        <source>Loading</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/Workshop/Workshop.qml" line="144"/>
        <source>Download now!</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/Workshop/Workshop.qml" line="149"/>
        <source>Downloading...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/Workshop/Workshop.qml" line="156"/>
        <source>Details</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/Workshop/Workshop.qml" line="178"/>
        <source>Open In Steam</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/Workshop/Workshop.qml" line="252"/>
        <source>Search for Wallpaper and Widgets...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/Workshop/Workshop.qml" line="288"/>
        <source>Ranked By Vote</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/Workshop/Workshop.qml" line="291"/>
        <source>Publication Date</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/Workshop/Workshop.qml" line="294"/>
        <source>Ranked By Trend</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/Workshop/Workshop.qml" line="297"/>
        <source>Favorited By Friends</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/Workshop/Workshop.qml" line="300"/>
        <source>Created By Friends</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/Workshop/Workshop.qml" line="303"/>
        <source>Created By Followed Users</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/Workshop/Workshop.qml" line="306"/>
        <source>Not Yet Rated</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/Workshop/Workshop.qml" line="309"/>
        <source>Total VotesAsc</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/Workshop/Workshop.qml" line="312"/>
        <source>Votes Up</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/Workshop/Workshop.qml" line="315"/>
        <source>Total Unique Subscriptions</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>WorkshopItem</name>
    <message>
        <location filename="../qml/Workshop/WorkshopItem.qml" line="184"/>
        <source>Download</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/Workshop/WorkshopItem.qml" line="302"/>
        <source>Successfully subscribed to Workshop Item!</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/Workshop/WorkshopItem.qml" line="412"/>
        <source>Download complete!</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>XMLNewsfeed</name>
    <message>
        <location filename="../qml/Community/XMLNewsfeed.qml" line="53"/>
        <source>News &amp; Patchnotes</source>
        <translation type="unfinished">Список изменений</translation>
    </message>
</context>
<context>
    <name>main</name>
    <message>
        <source>ScreenPlay - Double click to change you settings.</source>
        <translation type="obsolete">ScreenPlay - Двойной щелчок для изменения настроек.</translation>
    </message>
    <message>
        <source>Open ScreenPlay</source>
        <translation type="obsolete">Открыть ScreenPlay</translation>
    </message>
    <message>
        <source>Mute all</source>
        <translation type="obsolete">Отключить все звуки</translation>
    </message>
    <message>
        <source>Unmute all</source>
        <translation type="obsolete">Отключить все</translation>
    </message>
    <message>
        <source>Pause all</source>
        <translation type="obsolete">Остановить все</translation>
    </message>
    <message>
        <source>Play all</source>
        <translation type="obsolete">Играть всё</translation>
    </message>
    <message>
        <source>Quit</source>
        <translation type="obsolete">Выход</translation>
    </message>
</context>
</TS>
