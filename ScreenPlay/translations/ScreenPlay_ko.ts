<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="ko_KR">
<context>
    <name>Community</name>
    <message>
        <source>We use Stomt because it provides quick and easy feedback via I like/I wish. So you can easily give us feedback and speak your mind. We will read these wishes on a daily basis!</source>
        <translation type="vanished">쉽고 빠른 피드백을 위해 Stomt를 이용합니다. Stomt를 통해 여러분의 의견을 쉽게 전달하세요. 매일 확인합니다!</translation>
    </message>
    <message>
        <source>Open ScreenPlay Stomt page</source>
        <translation type="vanished">ScreenPlay Stomt 페이지 열기</translation>
    </message>
    <message>
        <source>Forums</source>
        <translation type="vanished">포럼</translation>
    </message>
    <message>
        <source>Blog</source>
        <translation type="vanished">블로그</translation>
    </message>
    <message>
        <source>Source Code</source>
        <translation type="vanished">소스 코드</translation>
    </message>
    <message>
        <source>Workshop</source>
        <translation type="vanished">워크샵</translation>
    </message>
    <message>
        <location filename="../qml/Community/Community.qml" line="34"/>
        <source>Wiki</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/Community/Community.qml" line="40"/>
        <source>Forum</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/Community/Community.qml" line="45"/>
        <source>Issue List</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/Community/Community.qml" line="50"/>
        <source>Release Notes</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/Community/Community.qml" line="55"/>
        <source>Contribution Guide</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/Community/Community.qml" line="60"/>
        <source>Steam Workshop</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>CommunityNavItem</name>
    <message>
        <location filename="../qml/Community/CommunityNavItem.qml" line="59"/>
        <source>Open in browser</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>Create</name>
    <message>
        <location filename="../qml/Create/Create.qml" line="101"/>
        <source>Create wallpapers and widgets for local usage or the steam workshop!</source>
        <translation>배경화면과 위젯을 만들어 사용하거나, 스팀 워크샵에 공유해보세요!</translation>
    </message>
</context>
<context>
    <name>CreateContent</name>
    <message>
        <location filename="../qml/Create/CreateContent.qml" line="15"/>
        <source>Create Widgets and Scenes</source>
        <translation>위젯과 움직이는 화면 만들기</translation>
    </message>
    <message>
        <location filename="../qml/Create/CreateContent.qml" line="29"/>
        <source>Create Empty Widget</source>
        <translation>빈 위젯 만들기</translation>
    </message>
    <message>
        <location filename="../qml/Create/CreateContent.qml" line="42"/>
        <source>Example Widgets and Scenes</source>
        <translation>위젯과 움직이는 화면 예제보기</translation>
    </message>
    <message>
        <location filename="../qml/Create/CreateContent.qml" line="65"/>
        <source>Empty HTML Wallpaper</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Simple clock widget</source>
        <translation type="vanished">간단한 시계 위젯</translation>
    </message>
    <message>
        <location filename="../qml/Create/CreateContent.qml" line="75"/>
        <source>Musik scene wallpaper visualizer</source>
        <translation>Musik 움직이는 화면 시각화</translation>
    </message>
    <message>
        <location filename="../qml/Create/CreateContent.qml" line="86"/>
        <source>Changing scene wallpaper via unsplash.com</source>
        <translation>unsplash.com을 통해 움직이는 화면 바꾸기</translation>
    </message>
</context>
<context>
    <name>CreateContentButton</name>
    <message>
        <location filename="../qml/Create/CreateContentButton.qml" line="114"/>
        <source>Not yet implemented. Stay tuned!</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>CreateEmptyHtmlWallpaper</name>
    <message>
        <location filename="../qml/Create/Wizards/CreateEmptyHtmlWallpaper/CreateEmptyHtmlWallpaper.qml" line="54"/>
        <source>This wizard lets you create a empty html based wallpaper. You can put anything you can imagine into this html file. For example this can be a three.js scene or a utility application written in javascript.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/Create/Wizards/CreateEmptyHtmlWallpaper/CreateEmptyHtmlWallpaper.qml" line="68"/>
        <source>Next</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/Create/Wizards/CreateEmptyHtmlWallpaper/CreateEmptyHtmlWallpaper.qml" line="119"/>
        <source>Create a html Wallpaper</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/Create/Wizards/CreateEmptyHtmlWallpaper/CreateEmptyHtmlWallpaper.qml" line="123"/>
        <source>General</source>
        <translation type="unfinished">일반</translation>
    </message>
    <message>
        <location filename="../qml/Create/Wizards/CreateEmptyHtmlWallpaper/CreateEmptyHtmlWallpaper.qml" line="131"/>
        <source>Wallpaper name</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/Create/Wizards/CreateEmptyHtmlWallpaper/CreateEmptyHtmlWallpaper.qml" line="145"/>
        <source>Copyright owner</source>
        <translation type="unfinished">저작권 소유자</translation>
    </message>
    <message>
        <location filename="../qml/Create/Wizards/CreateEmptyHtmlWallpaper/CreateEmptyHtmlWallpaper.qml" line="149"/>
        <source>License</source>
        <translation type="unfinished">라이선스</translation>
    </message>
    <message>
        <location filename="../qml/Create/Wizards/CreateEmptyHtmlWallpaper/CreateEmptyHtmlWallpaper.qml" line="172"/>
        <source>Tags</source>
        <translation type="unfinished">태그</translation>
    </message>
    <message>
        <location filename="../qml/Create/Wizards/CreateEmptyHtmlWallpaper/CreateEmptyHtmlWallpaper.qml" line="184"/>
        <source>Preview Image</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/Create/Wizards/CreateEmptyHtmlWallpaper/CreateEmptyHtmlWallpaper.qml" line="191"/>
        <source>You can set your own preview image here!</source>
        <translation type="unfinished">나만의 미리보기 이미지를 설정할 수 있습니다!</translation>
    </message>
    <message>
        <location filename="../qml/Create/Wizards/CreateEmptyHtmlWallpaper/CreateEmptyHtmlWallpaper.qml" line="216"/>
        <source>Abort</source>
        <translation type="unfinished">버리기</translation>
    </message>
    <message>
        <location filename="../qml/Create/Wizards/CreateEmptyHtmlWallpaper/CreateEmptyHtmlWallpaper.qml" line="228"/>
        <source>Save</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/Create/Wizards/CreateEmptyHtmlWallpaper/CreateEmptyHtmlWallpaper.qml" line="263"/>
        <source>Create Html Wallpaper...</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>CreateEmptyWidget</name>
    <message>
        <location filename="../qml/Create/Wizards/CreateEmptyWidget/CreateEmptyWidget.qml" line="17"/>
        <source>Create an empty widget</source>
        <translation>빈 위젯 만들기</translation>
    </message>
    <message>
        <location filename="../qml/Create/Wizards/CreateEmptyWidget/CreateEmptyWidget.qml" line="77"/>
        <source>General</source>
        <translation>일반</translation>
    </message>
    <message>
        <location filename="../qml/Create/Wizards/CreateEmptyWidget/CreateEmptyWidget.qml" line="85"/>
        <source>Widget name</source>
        <translation>위젯 이름</translation>
    </message>
    <message>
        <location filename="../qml/Create/Wizards/CreateEmptyWidget/CreateEmptyWidget.qml" line="98"/>
        <source>Copyright owner</source>
        <translation>저작권 소유자</translation>
    </message>
    <message>
        <location filename="../qml/Create/Wizards/CreateEmptyWidget/CreateEmptyWidget.qml" line="102"/>
        <source>Type</source>
        <translation>타입</translation>
    </message>
    <message>
        <location filename="../qml/Create/Wizards/CreateEmptyWidget/CreateEmptyWidget.qml" line="131"/>
        <source>License</source>
        <translation>라이선스</translation>
    </message>
    <message>
        <location filename="../qml/Create/Wizards/CreateEmptyWidget/CreateEmptyWidget.qml" line="154"/>
        <source>Tags</source>
        <translation>태그</translation>
    </message>
    <message>
        <location filename="../qml/Create/Wizards/CreateEmptyWidget/CreateEmptyWidget.qml" line="178"/>
        <source>Save</source>
        <translation>저장하기</translation>
    </message>
    <message>
        <location filename="../qml/Create/Wizards/CreateEmptyWidget/CreateEmptyWidget.qml" line="205"/>
        <source>Abort</source>
        <translation>버리기</translation>
    </message>
    <message>
        <location filename="../qml/Create/Wizards/CreateEmptyWidget/CreateEmptyWidget.qml" line="233"/>
        <source>Create Widget...</source>
        <translation>위젯 만들기...</translation>
    </message>
</context>
<context>
    <name>CreateImport</name>
    <message>
        <location filename="../qml/Create/CreateImport.qml" line="137"/>
        <source></source>
        <translation></translation>
    </message>
</context>
<context>
    <name>CreateUpload</name>
    <message>
        <location filename="../qml/Create/CreateUpload.qml" line="219"/>
        <source></source>
        <translation></translation>
    </message>
</context>
<context>
    <name>CreateWallpaperCodec</name>
    <message>
        <location filename="../qml/Create/Wizards/CreateWallpaper/CreateWallpaperCodec.qml" line="52"/>
        <source>Import a video</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/Create/Wizards/CreateWallpaper/CreateWallpaperCodec.qml" line="62"/>
        <source>Depending on your PC configuration it is better to convert your wallpaper to a specific video codec. If both have bad performance you can also try a QML wallpaper!</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/Create/Wizards/CreateWallpaper/CreateWallpaperCodec.qml" line="76"/>
        <source>Set your preffered video codec:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/Create/Wizards/CreateWallpaper/CreateWallpaperCodec.qml" line="108"/>
        <source>Open Documentation</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/Create/Wizards/CreateWallpaper/CreateWallpaperCodec.qml" line="125"/>
        <source>Next</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>CreateWallpaperResult</name>
    <message>
        <location filename="../qml/Create/Wizards/CreateWallpaper/CreateWallpaperResult.qml" line="18"/>
        <source>An error occurred!</source>
        <translation>에러가 발생했습니다!</translation>
    </message>
    <message>
        <location filename="../qml/Create/Wizards/CreateWallpaper/CreateWallpaperResult.qml" line="78"/>
        <source>Copy text to clipboard</source>
        <translation>클립보드에 글 복사하기</translation>
    </message>
    <message>
        <location filename="../qml/Create/Wizards/CreateWallpaper/CreateWallpaperResult.qml" line="88"/>
        <source>Back to create and send an error report!</source>
        <translation>생성 화면으로 돌아가고 에러 보고서를 보내주세요!</translation>
    </message>
</context>
<context>
    <name>CreateWallpaperVideoImportConvert</name>
    <message>
        <location filename="../qml/Create/Wizards/CreateWallpaper/CreateWallpaperVideoImportConvert.qml" line="54"/>
        <source>Generating preview image...</source>
        <translation>미리보기 이미지 생성 중...</translation>
    </message>
    <message>
        <location filename="../qml/Create/Wizards/CreateWallpaper/CreateWallpaperVideoImportConvert.qml" line="57"/>
        <source>Generating preview thumbnail image...</source>
        <translation>미리보기 썸네일 이미지 생성 중...</translation>
    </message>
    <message>
        <location filename="../qml/Create/Wizards/CreateWallpaper/CreateWallpaperVideoImportConvert.qml" line="64"/>
        <source>Generating 5 second preview video...</source>
        <translation>5초 미리보기 생성 중...</translation>
    </message>
    <message>
        <location filename="../qml/Create/Wizards/CreateWallpaper/CreateWallpaperVideoImportConvert.qml" line="67"/>
        <source>Generating preview gif...</source>
        <translation>미리보기 gif 생성 중...</translation>
    </message>
    <message>
        <location filename="../qml/Create/Wizards/CreateWallpaper/CreateWallpaperVideoImportConvert.qml" line="76"/>
        <source>Converting Audio...</source>
        <translation>소리 변환 중...</translation>
    </message>
    <message>
        <location filename="../qml/Create/Wizards/CreateWallpaper/CreateWallpaperVideoImportConvert.qml" line="79"/>
        <source>Converting Video... This can take some time!</source>
        <translation>동영상 변환 중... 잠시만 기다려주세요!</translation>
    </message>
    <message>
        <location filename="../qml/Create/Wizards/CreateWallpaper/CreateWallpaperVideoImportConvert.qml" line="83"/>
        <source>Converting Video ERROR!</source>
        <translation>동영상 변환 에러!</translation>
    </message>
    <message>
        <location filename="../qml/Create/Wizards/CreateWallpaper/CreateWallpaperVideoImportConvert.qml" line="86"/>
        <source>Analyse Video ERROR!</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/Create/Wizards/CreateWallpaper/CreateWallpaperVideoImportConvert.qml" line="114"/>
        <source>Convert a video to a wallpaper</source>
        <translation>동영상을 움직이는 배경으로 변환</translation>
    </message>
    <message>
        <location filename="../qml/Create/Wizards/CreateWallpaper/CreateWallpaperVideoImportConvert.qml" line="164"/>
        <source></source>
        <translation></translation>
    </message>
    <message>
        <location filename="../qml/Create/Wizards/CreateWallpaper/CreateWallpaperVideoImportConvert.qml" line="211"/>
        <source>Generating preview video...</source>
        <translation>미리보기 동영상 생성 중...</translation>
    </message>
    <message>
        <location filename="../qml/Create/Wizards/CreateWallpaper/CreateWallpaperVideoImportConvert.qml" line="224"/>
        <source>You can set your own preview image here!</source>
        <translation>나만의 미리보기 이미지를 설정할 수 있습니다!</translation>
    </message>
    <message>
        <location filename="../qml/Create/Wizards/CreateWallpaper/CreateWallpaperVideoImportConvert.qml" line="258"/>
        <source>Name (required!)</source>
        <translation>이름 (필수!)</translation>
    </message>
    <message>
        <location filename="../qml/Create/Wizards/CreateWallpaper/CreateWallpaperVideoImportConvert.qml" line="273"/>
        <source>Description</source>
        <translation>설명</translation>
    </message>
    <message>
        <location filename="../qml/Create/Wizards/CreateWallpaper/CreateWallpaperVideoImportConvert.qml" line="281"/>
        <source>Youtube URL</source>
        <translation>유튜브 URL</translation>
    </message>
    <message>
        <location filename="../qml/Create/Wizards/CreateWallpaper/CreateWallpaperVideoImportConvert.qml" line="309"/>
        <source>Abort</source>
        <translation>버리기</translation>
    </message>
    <message>
        <location filename="../qml/Create/Wizards/CreateWallpaper/CreateWallpaperVideoImportConvert.qml" line="322"/>
        <source>Save</source>
        <translation>저장</translation>
    </message>
    <message>
        <location filename="../qml/Create/Wizards/CreateWallpaper/CreateWallpaperVideoImportConvert.qml" line="359"/>
        <source>Save Wallpaper...</source>
        <translation>배경화면 저장하기...</translation>
    </message>
</context>
<context>
    <name>DefaultVideoControls</name>
    <message>
        <location filename="../qml/Monitors/DefaultVideoControls.qml" line="41"/>
        <source>Volume</source>
        <translation>음량</translation>
    </message>
    <message>
        <location filename="../qml/Monitors/DefaultVideoControls.qml" line="55"/>
        <source>Playback rate</source>
        <translation>재생 속도 비율</translation>
    </message>
    <message>
        <location filename="../qml/Monitors/DefaultVideoControls.qml" line="67"/>
        <source>Current Video Time</source>
        <translation>현재 비디오 시간</translation>
    </message>
    <message>
        <location filename="../qml/Monitors/DefaultVideoControls.qml" line="85"/>
        <source>Fill Mode</source>
        <translation>채우기 모드</translation>
    </message>
    <message>
        <location filename="../qml/Monitors/DefaultVideoControls.qml" line="110"/>
        <source>Stretch</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/Monitors/DefaultVideoControls.qml" line="113"/>
        <source>Fill</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/Monitors/DefaultVideoControls.qml" line="116"/>
        <source>Contain</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/Monitors/DefaultVideoControls.qml" line="119"/>
        <source>Cover</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/Monitors/DefaultVideoControls.qml" line="122"/>
        <source>Scale_Down</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>FFMPEGPopup</name>
    <message>
        <source>Begin downloading FFMPEG</source>
        <translation type="vanished">FFMPEG 다운로드 시작</translation>
    </message>
    <message>
        <source>FFMPEG download failed</source>
        <translation type="vanished">FFMPEG 다운로드 실패</translation>
    </message>
    <message>
        <source>FFMPEG download successful</source>
        <translation type="vanished">FFMPEG 다운로드 성공</translation>
    </message>
    <message>
        <source>Extracting FFMPEG</source>
        <translation type="vanished">FFMPEG 압축 해제 중</translation>
    </message>
    <message>
        <source>ERROR extracting ffmpeg from RAM</source>
        <translation type="vanished">RAM에서 FFMPEG 압축 해제 오류</translation>
    </message>
    <message>
        <source>ERROR extracing ffmpeg</source>
        <translation type="vanished">FFMPEG 압축 해제 오류</translation>
    </message>
    <message>
        <source>ERROR saving FFMPEG to disk</source>
        <translation type="vanished">FFMPEG 디스크 저장 오류</translation>
    </message>
    <message>
        <source>ERROR extracing FFPROBE</source>
        <translation type="vanished">FFPROBE 압축 해제 오류</translation>
    </message>
    <message>
        <source>ERROR saving FFPROBE to disk</source>
        <translation type="vanished">FFPROBE 디스크 저장 오류</translation>
    </message>
    <message>
        <source>Extraction successful</source>
        <translation type="vanished">압축 해제 성공</translation>
    </message>
    <message>
        <source>All done and ready to go!</source>
        <translation type="vanished">모두 마쳤습니다!</translation>
    </message>
    <message>
        <source>You cannot create Wallaper without FFMPEG installed!</source>
        <translation type="vanished">FFMPEG를 설치해야 배경화면을 만들 수 있습니다!</translation>
    </message>
    <message>
        <source>Abort</source>
        <translation type="vanished">버리기</translation>
    </message>
    <message>
        <source>Download FFMPEG</source>
        <translation type="vanished">FFMPEG 다운로드</translation>
    </message>
    <message>
        <source>Before we can start creating content you need to download FFMPEG</source>
        <translation type="vanished">FFMPEG를 설치해야 컨텐츠를 제작할 수 있습니다</translation>
    </message>
    <message>
        <source>&lt;b&gt;Why do we bother you with this?&lt;/b&gt;
&lt;br&gt;&lt;br&gt; Well its because of &lt;b&gt;copyright&lt;/b&gt; and many many &lt;b&gt;patents&lt;/b&gt;.
Files like .mp4 or .webm are containers for video and audio. Every audio
and video file is encoded with a certain codec. These can be open source
ceand free to use like &lt;a href=&apos;https://wikipedia.org/wiki/VP8&apos;&gt;VP8&lt;/a&gt; and the newer  &lt;a href=&apos;https://wikipedia.org/wiki/VP9&apos;&gt;VP9&lt;/a&gt; (the one YouTube uses for their web
ms)but there are also some proprietary ones like  &lt;a href=&apos;https://wikipedia.org/wiki/H.264/MPEG-4_AVC&apos;&gt;h264&lt;/a&gt; and the successor &lt;a href=&apos;https://wikipedia.org/wiki/High_Efficiency_Video_Coding&apos;&gt;h265&lt;/a&gt;.
&lt;br&gt;
&lt;br&gt;


 We as software developer now need to deal with stuff like this in a field wedo not have any expertise in. The desicion to enable only free codecs for content wasan easy one but we still need to provide a way for our user to import wallpaper without a hassle. We do not provide  &lt;a href=&apos;https://ffmpeg.org/&apos;&gt;FFMPEG&lt;/a&gt; for converting video and audio with ScreenPlay because we are not allowed to. We let the user download &lt;a href=&apos;https://ffmpeg.org/&apos;&gt;FFMPEG&lt;/a&gt; wich is perfectly fine!
&lt;br&gt;
Sorry for this little inconvenience :)
&lt;br&gt;
&lt;br&gt;
&lt;center&gt;
&lt;b&gt;
IF YOU DO NOT HAVE A INTERNET CONNECT YOU CAN SIMPLY PUT FFMPEG AND FFPROBE
IN THE SAME FOLDER AS YOUR SCREENPLAY EXECUTABLE!
&lt;/b&gt;
&lt;br&gt;
&lt;br&gt;
This is usually:
&lt;br&gt; C:Program Files (x86)SteamsteamappscommonScreenPlay
&lt;br&gt;
if you installed ScreenPlay via Steam!
&lt;/center&gt;
&lt;br&gt;
&lt;br&gt;
&lt;b&gt;~ Kelteseth | Elias Steurer&lt;/b&gt;</source>
        <translation type="vanished">&lt;b&gt;왜 나를 귀찮게 하나요?&lt;/b&gt;
&lt;br&gt;&lt;br&gt; 음, 그건 &lt;b&gt;저작권&lt;/b&gt;과 수많은 &lt;b&gt;특허&lt;/b&gt; 때문입니다.
mp4나 webm 같은 파일은 영상과 오디오를 저장하는 컨테이너입니다.
오디오와 영상 파일은 반드시 특정 코덱으로 인코딩됩니다.
&lt;a href=&apos;https://wikipedia.org/wiki/VP8&apos;&gt;VP8&lt;/a&gt; 혹은 새로운 버전인 &lt;a href=&apos;https://wikipedia.org/wiki/VP9&apos;&gt;VP9(유튜브에서 동영상을 볼 때 사용됩니다)&lt;/a&gt;와 같은 무료 오픈소스도 있지만,
소유권이 있는 &lt;a href=&apos;https://wikipedia.org/wiki/H.264/MPEG-4_AVC&apos;&gt;h264&lt;/a&gt;와 다음 버전인 &lt;a href=&apos;https://wikipedia.org/wiki/High_Efficiency_Video_Coding&apos;&gt;h265&lt;/a&gt;와 같은 것도 있습니다.
&lt;br&gt;
&lt;br&gt;


소프트웨어 개발자지만, 전문지식이 없는 이런 분야가 있기에, 선택할 수 밖에 없었습니다. 무료 코덱 컨텐츠만을 이용하는 것은 쉽지만, 여전히 사용자들이 불편없이 배경화면을 불러오는 방법을 제공할 필요가 있었습니다. 저희는 &lt;a href=&apos;https://ffmpeg.org/&apos;&gt;FFMPEG&lt;/a&gt;를 배포할 권한이 없지만, 여러분이 직접 &lt;a href=&apos;https://ffmpeg.org/&apos;&gt;FFMPEG&lt;/a&gt;를 다운로드 하는 것은 전혀 문제가 없습니다!
&lt;br&gt;
불편함을 견뎌주셔서 감사합니다 :)
&lt;br&gt;
&lt;br&gt;
&lt;center&gt;
&lt;b&gt;
인터넷 연결이 없다면 ScreenPlay EXE 파일과 같은 위치에 FFMPEG와 FFPROBE를 넣어주세요
&lt;/b&gt;
&lt;br&gt;
&lt;br&gt;
기본 파일 위치는 아래와 같습니다:
&lt;br&gt; C:Program Files (x86)SteamsteamappscommonScreenPlay
&lt;br&gt;
Steam을 통해 설치하셨을때의 기본 위치입니다!
&lt;/center&gt;
&lt;br&gt;
&lt;br&gt;
&lt;b&gt;~ Kelteseth | Elias Steurer&lt;/b&gt;</translation>
    </message>
    <message>
        <source>Not now!</source>
        <translation type="vanished">지금은 안 할래요!</translation>
    </message>
    <message>
        <source>You can now start creating content!</source>
        <translation type="vanished">이제 콘텐츠를 제작할 수 있습니다!</translation>
    </message>
    <message>
        <source>Start!</source>
        <translation type="vanished">시작!</translation>
    </message>
</context>
<context>
    <name>Footer</name>
    <message>
        <location filename="../qml/Create/Footer.qml" line="23"/>
        <source>QML Quickstart Guide</source>
        <translation>QML 길라잡이</translation>
    </message>
    <message>
        <location filename="../qml/Create/Footer.qml" line="34"/>
        <source>Documentation</source>
        <translation>문서</translation>
    </message>
    <message>
        <location filename="../qml/Create/Footer.qml" line="45"/>
        <source>Forums</source>
        <translation>포럼</translation>
    </message>
    <message>
        <location filename="../qml/Create/Footer.qml" line="56"/>
        <source>Workshop</source>
        <translation>워크샵</translation>
    </message>
</context>
<context>
    <name>Headline</name>
    <message>
        <location filename="../qml/Common/Headline.qml" line="16"/>
        <source>Headline</source>
        <translation type="unfinished">헤드라인</translation>
    </message>
</context>
<context>
    <name>ImageSelector</name>
    <message>
        <location filename="../qml/Common/ImageSelector.qml" line="142"/>
        <source>Clear</source>
        <translation>모두 해제</translation>
    </message>
    <message>
        <location filename="../qml/Common/ImageSelector.qml" line="160"/>
        <source>Select Preview Image</source>
        <translation>미리보기 이미지 선택</translation>
    </message>
</context>
<context>
    <name>ImportContent</name>
    <message>
        <location filename="../qml/Create/ImportContent.qml" line="22"/>
        <source>Import Content</source>
        <translation>콘텐츠 불러오기</translation>
    </message>
    <message>
        <location filename="../qml/Create/ImportContent.qml" line="75"/>
        <source>Import video</source>
        <translation>비디오 불러오기</translation>
    </message>
    <message>
        <source>FFMPEG Needed for import</source>
        <translation type="vanished">불러오려면 FFMPEG가 필요합니다</translation>
    </message>
    <message>
        <source>Import ThreeJs Scene</source>
        <translation type="vanished">ThreeJs 화면 불러오기</translation>
    </message>
    <message>
        <location filename="../qml/Create/ImportContent.qml" line="161"/>
        <source>Upload Exsisting Project to Steam</source>
        <translation>기존 프로젝트를 스팀에 업로드</translation>
    </message>
</context>
<context>
    <name>Installed</name>
    <message>
        <location filename="../qml/Installed/Installed.qml" line="99"/>
        <source>Refreshing!</source>
        <translation>새로고침 중!</translation>
    </message>
    <message>
        <location filename="../qml/Installed/Installed.qml" line="102"/>
        <location filename="../qml/Installed/Installed.qml" line="124"/>
        <source>Pull to refresh!</source>
        <translation>당겨서 새로고침!</translation>
    </message>
    <message>
        <location filename="../qml/Installed/Installed.qml" line="141"/>
        <source>Get more Wallpaper &amp; Widgets via the Steam workshop!</source>
        <translation>스팀 워크샵에서 더 많은 배경화면 &amp; 위젯을 받아보세요!</translation>
    </message>
    <message>
        <location filename="../qml/Installed/Installed.qml" line="217"/>
        <source>Open containing folder</source>
        <translation type="unfinished">폴더 열기</translation>
    </message>
    <message>
        <location filename="../qml/Installed/Installed.qml" line="225"/>
        <source>Deinstall Item</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/Installed/Installed.qml" line="234"/>
        <source>Open workshop Page</source>
        <translation type="unfinished">워크샵 페이지 열기</translation>
    </message>
    <message>
        <location filename="../qml/Installed/Installed.qml" line="245"/>
        <source>Are you sure you want to delete this item?</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>All</source>
        <translation type="vanished">전체</translation>
    </message>
    <message>
        <source>Videos</source>
        <translation type="vanished">비디오</translation>
    </message>
    <message>
        <source>Scenes</source>
        <translation type="vanished">화면</translation>
    </message>
    <message>
        <source>Widgets</source>
        <translation type="vanished">위젯</translation>
    </message>
    <message>
        <location filename="../qml/Installed/Installed.qml" line="329"/>
        <source></source>
        <translation></translation>
    </message>
    <message>
        <source>Search for Wallpaper &amp; Widgets</source>
        <translation type="vanished">배경화면 &amp; 위젯 검색</translation>
    </message>
</context>
<context>
    <name>InstalledWelcomeScreen</name>
    <message>
        <location filename="../qml/Installed/InstalledWelcomeScreen.qml" line="54"/>
        <source>Get free Widgets and Wallpaper via the Steam Workshop</source>
        <translation>스팀 워크샵에서 무료 위젯과 배경화면을 받아보세요</translation>
    </message>
    <message>
        <location filename="../qml/Installed/InstalledWelcomeScreen.qml" line="84"/>
        <source>Browse the Steam Workshop</source>
        <translation>스팀 워크샵 둘러보기</translation>
    </message>
</context>
<context>
    <name>Monitors</name>
    <message>
        <location filename="../qml/Monitors/Monitors.qml" line="67"/>
        <source>Wallpaper Configuration</source>
        <translation>배경화면 설정</translation>
    </message>
    <message>
        <location filename="../qml/Monitors/Monitors.qml" line="132"/>
        <source>Remove selected</source>
        <translation>선택 목록 삭제</translation>
    </message>
    <message>
        <location filename="../qml/Monitors/Monitors.qml" line="144"/>
        <location filename="../qml/Monitors/Monitors.qml" line="158"/>
        <source>Remove </source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/Monitors/Monitors.qml" line="145"/>
        <source>Wallpapers</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/Monitors/Monitors.qml" line="159"/>
        <source>Widgets</source>
        <translation type="unfinished">위젯</translation>
    </message>
    <message>
        <source>Remove all Wallpapers</source>
        <translation type="vanished">모든 배경화면 제거</translation>
    </message>
    <message>
        <source>Remove all Widgets</source>
        <translation type="vanished">모든 위젯 제거</translation>
    </message>
</context>
<context>
    <name>MonitorsProjectSettingItem</name>
    <message>
        <location filename="../qml/Monitors/MonitorsProjectSettingItem.qml" line="132"/>
        <source>Set color</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/Monitors/MonitorsProjectSettingItem.qml" line="155"/>
        <source>Please choose a color</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>Navigation</name>
    <message>
        <location filename="../qml/Installed/Navigation.qml" line="57"/>
        <source>All</source>
        <translation type="unfinished">전체</translation>
    </message>
    <message>
        <location filename="../qml/Installed/Navigation.qml" line="73"/>
        <source>Scenes</source>
        <translation type="unfinished">화면</translation>
    </message>
    <message>
        <location filename="../qml/Installed/Navigation.qml" line="89"/>
        <source>Videos</source>
        <translation type="unfinished">비디오</translation>
    </message>
    <message>
        <location filename="../qml/Installed/Navigation.qml" line="105"/>
        <source>Widgets</source>
        <translation type="unfinished">위젯</translation>
    </message>
    <message>
        <location filename="../qml/Workshop/Navigation.qml" line="46"/>
        <source> Subscribed items: </source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/Workshop/Navigation.qml" line="87"/>
        <source>Upload to the Steam Workshop</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>NavigationWallpaperConfiguration</name>
    <message>
        <location filename="../qml/Navigation/NavigationWallpaperConfiguration.qml" line="70"/>
        <source>Configurate active Wallpaper or Widgets</source>
        <translation>활성화된 배경화면 혹은 위젯 설정하기</translation>
    </message>
    <message>
        <location filename="../qml/Navigation/NavigationWallpaperConfiguration.qml" line="72"/>
        <source>No active Wallpaper or Widgets</source>
        <translation>활성화된 배경화면 및 위젯이 없습니다</translation>
    </message>
</context>
<context>
    <name>PopupOffline</name>
    <message>
        <location filename="../qml/Workshop/PopupOffline.qml" line="28"/>
        <source>You need to run Steam for this :)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/Workshop/PopupOffline.qml" line="37"/>
        <source>Back</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>PopupSteamWorkshopAgreement</name>
    <message>
        <location filename="../qml/Workshop/upload/PopupSteamWorkshopAgreement.qml" line="22"/>
        <source>Abort Upload.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/Workshop/upload/PopupSteamWorkshopAgreement.qml" line="30"/>
        <source>I Agree to the Steam Workshop Agreement</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>SaveNotification</name>
    <message>
        <location filename="../qml/Monitors/SaveNotification.qml" line="40"/>
        <source>Profile saved successfully!</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>ScreenPlayItem</name>
    <message>
        <source>Open containing folder</source>
        <translation type="vanished">폴더 열기</translation>
    </message>
    <message>
        <source>Open workshop Page</source>
        <translation type="vanished">워크샵 페이지 열기</translation>
    </message>
</context>
<context>
    <name>Search</name>
    <message>
        <location filename="../qml/Common/Search.qml" line="45"/>
        <source>Search for Wallpaper &amp; Widgets</source>
        <translation type="unfinished">배경화면 &amp; 위젯 검색</translation>
    </message>
</context>
<context>
    <name>Settings</name>
    <message>
        <location filename="../qml/Settings/Settings.qml" line="59"/>
        <source>General</source>
        <translation>일반</translation>
    </message>
    <message>
        <location filename="../qml/Settings/Settings.qml" line="74"/>
        <source>Autostart</source>
        <translation>자동 시작</translation>
    </message>
    <message>
        <location filename="../qml/Settings/Settings.qml" line="75"/>
        <source>ScreenPlay will start with Windows and will setup your Desktop every time for you.</source>
        <translation>컴퓨터 시작시에 자동 시작됩니다.</translation>
    </message>
    <message>
        <location filename="../qml/Settings/Settings.qml" line="83"/>
        <source>High priority Autostart</source>
        <translation>높은 우선 순위 자동시작</translation>
    </message>
    <message>
        <location filename="../qml/Settings/Settings.qml" line="86"/>
        <source>This options grants ScreenPlay a higher autostart priority than other apps.</source>
        <translation>이 옵션으로 ScreenPlay에 더 높은 우선순위를 설정할 수 있습니다.</translation>
    </message>
    <message>
        <location filename="../qml/Settings/Settings.qml" line="95"/>
        <source>Send anonymous crash reports and statistics</source>
        <translation>익명으로 통계 및 보고서 보내기</translation>
    </message>
    <message>
        <location filename="../qml/Settings/Settings.qml" line="96"/>
        <source>Help us make ScreenPlay faster and more stable. All collected data is purely anonymous and only used for development purposes!</source>
        <translation>ScreenPlay가 더 빠르고 더 안전해지도록 도와주세요. 수집된 정보는 완벽히 익명으로 처리되며 개발 목적으로만 사용됩니다!</translation>
    </message>
    <message>
        <location filename="../qml/Settings/Settings.qml" line="105"/>
        <source>Set save location</source>
        <translation>파일 저장 위치 설정</translation>
    </message>
    <message>
        <location filename="../qml/Settings/Settings.qml" line="110"/>
        <source>Your storage path is empty!</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/Settings/Settings.qml" line="116"/>
        <source>Set location</source>
        <translation>파일 위치 설정</translation>
    </message>
    <message>
        <location filename="../qml/Settings/Settings.qml" line="131"/>
        <source>Important: Changing this directory has no effect on the workshop download path. ScreenPlay only supports having one content folder!</source>
        <translation>중요: 이 위치를 변경하더라도 워크샵 다운로드 위치가 변경되는 것은 아닙니다. ScreenPlay는 단 하나의 컨텐츠 폴더만 지원합니다!</translation>
    </message>
    <message>
        <location filename="../qml/Settings/Settings.qml" line="149"/>
        <source>Language</source>
        <translation>언어</translation>
    </message>
    <message>
        <location filename="../qml/Settings/Settings.qml" line="150"/>
        <source>Set the ScreenPlay UI Language</source>
        <translation>ScreenPlay UI 언어 설정</translation>
    </message>
    <message>
        <location filename="../qml/Settings/Settings.qml" line="165"/>
        <source>English</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/Settings/Settings.qml" line="168"/>
        <source>German</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/Settings/Settings.qml" line="174"/>
        <source>Russian</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/Settings/Settings.qml" line="177"/>
        <source>French</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/Settings/Settings.qml" line="180"/>
        <source>Spanish</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/Settings/Settings.qml" line="183"/>
        <source>Korean</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/Settings/Settings.qml" line="186"/>
        <source>Vietnamese</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/Settings/Settings.qml" line="194"/>
        <source>Theme</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/Settings/Settings.qml" line="195"/>
        <source>Switch dark/light theme</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/Settings/Settings.qml" line="209"/>
        <source>System Default</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/Settings/Settings.qml" line="212"/>
        <source>Dark</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/Settings/Settings.qml" line="215"/>
        <source>Light</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/Settings/Settings.qml" line="225"/>
        <source>Performance</source>
        <translation>성능</translation>
    </message>
    <message>
        <location filename="../qml/Settings/Settings.qml" line="240"/>
        <source>Pause wallpaper video rendering while another app is in the foreground</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/Settings/Settings.qml" line="241"/>
        <source>We disable the video rendering (not the audio!) for the best performance. If you have problem you can disable this behaviour here. Wallpaper restart required!</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Pause wallpaper while ingame</source>
        <translation type="vanished">게임 중에는 배경화면 일시 정지</translation>
    </message>
    <message>
        <source>To maximise your framerates ingame, you can enable this setting to pause all active wallpapers!</source>
        <translation type="vanished">즐기시는 게임의 프레임율을 향상시키려면, 이 기능을 켜서 활성화된 모든 배경화면을 일시 정지 시키세요!</translation>
    </message>
    <message>
        <location filename="../qml/Settings/Settings.qml" line="171"/>
        <source>Chinese - Simplified</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/Settings/Settings.qml" line="251"/>
        <source>Default Fill Mode</source>
        <translation>기본 채우기 설정</translation>
    </message>
    <message>
        <location filename="../qml/Settings/Settings.qml" line="252"/>
        <source>Set this property to define how the video is scaled to fit the target area.</source>
        <translation>이 값으로 화면에 영상을 어떻게 채울지 설정하세요.</translation>
    </message>
    <message>
        <location filename="../qml/Settings/Settings.qml" line="264"/>
        <source>Stretch</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/Settings/Settings.qml" line="267"/>
        <source>Fill</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/Settings/Settings.qml" line="270"/>
        <source>Contain</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/Settings/Settings.qml" line="273"/>
        <source>Cover</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/Settings/Settings.qml" line="276"/>
        <source>Scale-Down</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/Settings/Settings.qml" line="286"/>
        <source>About</source>
        <translation>알아보기</translation>
    </message>
    <message>
        <location filename="../qml/Settings/Settings.qml" line="314"/>
        <source>Thank you for using ScreenPlay</source>
        <translation>ScreenPlay를 사용해주셔서 감사합니다</translation>
    </message>
    <message>
        <location filename="../qml/Settings/Settings.qml" line="329"/>
        <source>Hi, I&apos;m Elias Steurer also known as Kelteseth and I&apos;m the developer of ScreenPlay. Thank you for using my software. You can follow me to receive updates about ScreenPlay here:</source>
        <translation>안녕하세요, ScreenPlay를 개발한, Kelteseth 닉네임을 쓰고 있는 Elias Steurer입니다.  제 소프트웨어를 사용해주셔서 감사합니다. 저를 팔로우하셔서 ScreenPlay의 업데이트 내용을 확인하실 수 있습니다 클릭:</translation>
    </message>
    <message>
        <location filename="../qml/Settings/Settings.qml" line="418"/>
        <source>Version</source>
        <translation>버전</translation>
    </message>
    <message>
        <location filename="../qml/Settings/Settings.qml" line="419"/>
        <source>ScreenPlay Build Version </source>
        <translation>ScreenPlay 빌드 버전 </translation>
    </message>
    <message>
        <location filename="../qml/Settings/Settings.qml" line="421"/>
        <source>Open Changelog</source>
        <translation>변경 이력 열기</translation>
    </message>
    <message>
        <location filename="../qml/Settings/Settings.qml" line="428"/>
        <source>Third Party Software</source>
        <translation>타사 소프트웨어</translation>
    </message>
    <message>
        <location filename="../qml/Settings/Settings.qml" line="429"/>
        <source>ScreenPlay would not be possible without the work of others. A big thank you to: </source>
        <translation>ScreenPlay 기여자 여러분의 도움에 의해 완성되었습니다. 여러분께 감사드립니다: </translation>
    </message>
    <message>
        <location filename="../qml/Settings/Settings.qml" line="430"/>
        <source>Licenses</source>
        <translation>라이선스</translation>
    </message>
    <message>
        <location filename="../qml/Settings/Settings.qml" line="448"/>
        <location filename="../qml/Settings/Settings.qml" line="450"/>
        <source>Debug Messages</source>
        <translation>디버그 메시지</translation>
    </message>
    <message>
        <location filename="../qml/Settings/Settings.qml" line="449"/>
        <source>If your ScreenPlay missbehaves this is a good way to look for answers. This shows all logs and warning during runtime.</source>
        <translation>ScreenPlay가 올바르게 작동하지 않고 있다면, 이것이 해답이 될 수 있습니다. 이것은 런타임동안의 모든 이력과 경고를 포함하고 있습니다.</translation>
    </message>
    <message>
        <location filename="../qml/Settings/Settings.qml" line="461"/>
        <source>Data Protection</source>
        <translation>데이터 보호</translation>
    </message>
    <message>
        <location filename="../qml/Settings/Settings.qml" line="462"/>
        <source>We use you data very carefully to improve ScreenPlay. We do not sell or share this (anonymous) information with others!</source>
        <translation>ScreenPlay 향상을 위해 여러분의 익명 데이터를 신중히 사용합니다. 절대 정보를 팔거나 공유하지 않습니다!</translation>
    </message>
    <message>
        <location filename="../qml/Settings/Settings.qml" line="463"/>
        <source>Privacy</source>
        <translation>개인 정보</translation>
    </message>
</context>
<context>
    <name>SettingsExpander</name>
    <message>
        <location filename="../qml/Settings/SettingsExpander.qml" line="51"/>
        <source>Copy text to clipboard</source>
        <translation>클립보드에 글 복사</translation>
    </message>
</context>
<context>
    <name>Sidebar</name>
    <message>
        <location filename="../qml/Installed/Sidebar.qml" line="99"/>
        <source>Set Widget</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/Installed/Sidebar.qml" line="208"/>
        <source>Headline</source>
        <translation>헤드라인</translation>
    </message>
    <message>
        <location filename="../qml/Installed/Sidebar.qml" line="259"/>
        <source>Select a Monitor to display the content</source>
        <translation>콘텐츠를 표시할 모니터를 선택하세요</translation>
    </message>
    <message>
        <location filename="../qml/Installed/Sidebar.qml" line="294"/>
        <source>Set Volume</source>
        <translation>음량 설정</translation>
    </message>
    <message>
        <location filename="../qml/Installed/Sidebar.qml" line="304"/>
        <source>Fill Mode</source>
        <translation>채우기 모드</translation>
    </message>
    <message>
        <location filename="../qml/Installed/Sidebar.qml" line="327"/>
        <source>Stretch</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/Installed/Sidebar.qml" line="330"/>
        <source>Fill</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/Installed/Sidebar.qml" line="333"/>
        <source>Contain</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/Installed/Sidebar.qml" line="336"/>
        <source>Cover</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/Installed/Sidebar.qml" line="339"/>
        <source>Scale-Down</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Set wallpaper</source>
        <translation type="vanished">배경화면 설정</translation>
    </message>
    <message>
        <location filename="../qml/Installed/Sidebar.qml" line="96"/>
        <source>Set Wallpaper</source>
        <translation>배경화면 설정</translation>
    </message>
    <message>
        <source>Create Widget</source>
        <translation type="vanished">위젯 만들기</translation>
    </message>
    <message>
        <source>Create Wallpaper</source>
        <translation type="vanished">배경화면 만들기</translation>
    </message>
    <message>
        <location filename="../qml/Workshop/Sidebar.qml" line="91"/>
        <source>Project size: </source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/Workshop/Sidebar.qml" line="91"/>
        <source> MB</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/Workshop/Sidebar.qml" line="97"/>
        <source>No description...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/Workshop/Sidebar.qml" line="239"/>
        <source>Click here if you like the content</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/Workshop/Sidebar.qml" line="253"/>
        <source>Click here if you do not like the content</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/Workshop/Sidebar.qml" line="288"/>
        <source>Tags: </source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/Workshop/Sidebar.qml" line="313"/>
        <source>Subscribtions: </source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/Workshop/Sidebar.qml" line="319"/>
        <source>Open In Steam</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/Workshop/Sidebar.qml" line="362"/>
        <source>Subscribed!</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/Workshop/Sidebar.qml" line="362"/>
        <source>Subscribe</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>TagSelector</name>
    <message>
        <location filename="../qml/Common/TagSelector.qml" line="12"/>
        <source>Save</source>
        <translation>저장</translation>
    </message>
    <message>
        <location filename="../qml/Common/TagSelector.qml" line="15"/>
        <source>Add tag</source>
        <translation>태그 추가</translation>
    </message>
    <message>
        <location filename="../qml/Common/TagSelector.qml" line="109"/>
        <source>Cancel</source>
        <translation>취소</translation>
    </message>
    <message>
        <location filename="../qml/Common/TagSelector.qml" line="128"/>
        <source>Add Tag</source>
        <translation>태그 추가</translation>
    </message>
</context>
<context>
    <name>TrayIcon</name>
    <message>
        <location filename="../qml/Common/TrayIcon.qml" line="9"/>
        <source>ScreenPlay - Double click to change you settings.</source>
        <translation type="unfinished">ScreenPlay - 더블 클릭하여 설정을 변경하세요.</translation>
    </message>
    <message>
        <location filename="../qml/Common/TrayIcon.qml" line="28"/>
        <source>Open ScreenPlay</source>
        <translation type="unfinished">ScreenPlay 열기</translation>
    </message>
    <message>
        <location filename="../qml/Common/TrayIcon.qml" line="36"/>
        <location filename="../qml/Common/TrayIcon.qml" line="40"/>
        <source>Mute all</source>
        <translation type="unfinished">모두 음소거</translation>
    </message>
    <message>
        <location filename="../qml/Common/TrayIcon.qml" line="45"/>
        <source>Unmute all</source>
        <translation type="unfinished">모두 음소거 해제</translation>
    </message>
    <message>
        <location filename="../qml/Common/TrayIcon.qml" line="54"/>
        <location filename="../qml/Common/TrayIcon.qml" line="58"/>
        <source>Pause all</source>
        <translation type="unfinished">모두 일시정지</translation>
    </message>
    <message>
        <location filename="../qml/Common/TrayIcon.qml" line="63"/>
        <source>Play all</source>
        <translation type="unfinished">모두 재생</translation>
    </message>
    <message>
        <location filename="../qml/Common/TrayIcon.qml" line="70"/>
        <source>Quit</source>
        <translation type="unfinished">종료</translation>
    </message>
</context>
<context>
    <name>UploadProject</name>
    <message>
        <location filename="../qml/Workshop/upload/UploadProject.qml" line="63"/>
        <source>Upload Wallpaper/Widgets to Steam</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/Workshop/upload/UploadProject.qml" line="137"/>
        <source>Abort</source>
        <translation type="unfinished">버리기</translation>
    </message>
    <message>
        <location filename="../qml/Workshop/upload/UploadProject.qml" line="152"/>
        <source>Upload Projects</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/Workshop/upload/UploadProject.qml" line="212"/>
        <source>Finish</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>UploadProjectBigItem</name>
    <message>
        <location filename="../qml/Workshop/upload/UploadProjectBigItem.qml" line="114"/>
        <source>Type: </source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/Workshop/upload/UploadProjectBigItem.qml" line="121"/>
        <source>Open Folder</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/Workshop/upload/UploadProjectBigItem.qml" line="141"/>
        <source>Invalid Project!</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>UploadProjectItem</name>
    <message>
        <location filename="../qml/Workshop/upload/UploadProjectItem.qml" line="43"/>
        <source>Fail</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/Workshop/upload/UploadProjectItem.qml" line="46"/>
        <source>No Connection</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/Workshop/upload/UploadProjectItem.qml" line="49"/>
        <source>Invalid Password</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/Workshop/upload/UploadProjectItem.qml" line="52"/>
        <source>Logged In Elsewhere</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/Workshop/upload/UploadProjectItem.qml" line="55"/>
        <source>Invalid Protocol Version</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/Workshop/upload/UploadProjectItem.qml" line="58"/>
        <source>Invalid Param</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/Workshop/upload/UploadProjectItem.qml" line="61"/>
        <source>File Not Found</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/Workshop/upload/UploadProjectItem.qml" line="64"/>
        <source>Busy</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/Workshop/upload/UploadProjectItem.qml" line="67"/>
        <source>Invalid State</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/Workshop/upload/UploadProjectItem.qml" line="70"/>
        <source>Invalid Name</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/Workshop/upload/UploadProjectItem.qml" line="73"/>
        <source>Invalid Email</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/Workshop/upload/UploadProjectItem.qml" line="76"/>
        <source>Duplicate Name</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/Workshop/upload/UploadProjectItem.qml" line="79"/>
        <source>Access Denied</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/Workshop/upload/UploadProjectItem.qml" line="82"/>
        <source>Timeout</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/Workshop/upload/UploadProjectItem.qml" line="85"/>
        <source>Banned</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/Workshop/upload/UploadProjectItem.qml" line="88"/>
        <source>Account Not Found</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/Workshop/upload/UploadProjectItem.qml" line="91"/>
        <source>Invalid SteamID</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/Workshop/upload/UploadProjectItem.qml" line="94"/>
        <source>Service Unavailable</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/Workshop/upload/UploadProjectItem.qml" line="97"/>
        <source>Not Logged On</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/Workshop/upload/UploadProjectItem.qml" line="100"/>
        <source>Pending</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/Workshop/upload/UploadProjectItem.qml" line="103"/>
        <source>Encryption Failure</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/Workshop/upload/UploadProjectItem.qml" line="106"/>
        <source>Insufficient Privilege</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/Workshop/upload/UploadProjectItem.qml" line="109"/>
        <source>Limit Exceeded</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/Workshop/upload/UploadProjectItem.qml" line="112"/>
        <source>Revoked</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/Workshop/upload/UploadProjectItem.qml" line="115"/>
        <source>Expired</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/Workshop/upload/UploadProjectItem.qml" line="118"/>
        <source>Already Redeemed</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/Workshop/upload/UploadProjectItem.qml" line="121"/>
        <source>Duplicate Request</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/Workshop/upload/UploadProjectItem.qml" line="124"/>
        <source>Already Owned</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/Workshop/upload/UploadProjectItem.qml" line="127"/>
        <source>IP Not Found</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/Workshop/upload/UploadProjectItem.qml" line="130"/>
        <source>Persist Failed</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/Workshop/upload/UploadProjectItem.qml" line="133"/>
        <source>Locking Failed</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/Workshop/upload/UploadProjectItem.qml" line="136"/>
        <source>Logon Session Replaced</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/Workshop/upload/UploadProjectItem.qml" line="139"/>
        <source>Connect Failed</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/Workshop/upload/UploadProjectItem.qml" line="142"/>
        <source>Handshake Failed</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/Workshop/upload/UploadProjectItem.qml" line="145"/>
        <source>IO Failure</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/Workshop/upload/UploadProjectItem.qml" line="148"/>
        <source>Remote Disconnect</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/Workshop/upload/UploadProjectItem.qml" line="151"/>
        <source>Shopping Cart Not Found</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/Workshop/upload/UploadProjectItem.qml" line="154"/>
        <source>Blocked</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/Workshop/upload/UploadProjectItem.qml" line="157"/>
        <source>Ignored</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/Workshop/upload/UploadProjectItem.qml" line="160"/>
        <source>No Match</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/Workshop/upload/UploadProjectItem.qml" line="163"/>
        <source>Account Disabled</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/Workshop/upload/UploadProjectItem.qml" line="166"/>
        <source>Service ReadOnly</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/Workshop/upload/UploadProjectItem.qml" line="169"/>
        <source>Account Not Featured</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/Workshop/upload/UploadProjectItem.qml" line="172"/>
        <source>Administrator OK</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/Workshop/upload/UploadProjectItem.qml" line="175"/>
        <source>Content Version</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/Workshop/upload/UploadProjectItem.qml" line="178"/>
        <source>Try Another CM</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/Workshop/upload/UploadProjectItem.qml" line="181"/>
        <source>Password Required T oKick Session</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/Workshop/upload/UploadProjectItem.qml" line="184"/>
        <source>Already Logged In Elsewhere</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/Workshop/upload/UploadProjectItem.qml" line="187"/>
        <source>Suspended</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/Workshop/upload/UploadProjectItem.qml" line="190"/>
        <source>Cancelled</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/Workshop/upload/UploadProjectItem.qml" line="193"/>
        <source>Data Corruption</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/Workshop/upload/UploadProjectItem.qml" line="196"/>
        <source>Disk Full</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/Workshop/upload/UploadProjectItem.qml" line="199"/>
        <source>Remote Call Failed</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/Workshop/upload/UploadProjectItem.qml" line="202"/>
        <source>Password Unset</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/Workshop/upload/UploadProjectItem.qml" line="205"/>
        <source>External Account Unlinked</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/Workshop/upload/UploadProjectItem.qml" line="208"/>
        <source>PSN Ticket Invalid</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/Workshop/upload/UploadProjectItem.qml" line="211"/>
        <source>External Account Already Linked</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/Workshop/upload/UploadProjectItem.qml" line="214"/>
        <source>Remote File Conflict</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/Workshop/upload/UploadProjectItem.qml" line="217"/>
        <source>Illegal Password</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/Workshop/upload/UploadProjectItem.qml" line="220"/>
        <source>Same As Previous Value</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/Workshop/upload/UploadProjectItem.qml" line="223"/>
        <source>Account Logon Denied</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/Workshop/upload/UploadProjectItem.qml" line="226"/>
        <source>Cannot Use Old Password</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/Workshop/upload/UploadProjectItem.qml" line="229"/>
        <source>Invalid Login AuthCode</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/Workshop/upload/UploadProjectItem.qml" line="232"/>
        <source>Account Logon Denied No Mail</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/Workshop/upload/UploadProjectItem.qml" line="235"/>
        <source>Hardware Not Capable Of IPT</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/Workshop/upload/UploadProjectItem.qml" line="238"/>
        <source>IPT Init Error</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/Workshop/upload/UploadProjectItem.qml" line="241"/>
        <source>Parental Control Restricted</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/Workshop/upload/UploadProjectItem.qml" line="244"/>
        <source>Facebook Query Error</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/Workshop/upload/UploadProjectItem.qml" line="247"/>
        <source>Expired Login Auth Code</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/Workshop/upload/UploadProjectItem.qml" line="250"/>
        <source>IP Login Restriction Failed</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/Workshop/upload/UploadProjectItem.qml" line="253"/>
        <source>Account Locked Down</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/Workshop/upload/UploadProjectItem.qml" line="256"/>
        <source>Account Logon Denied Verified Email Required</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/Workshop/upload/UploadProjectItem.qml" line="259"/>
        <source>No MatchingURL</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/Workshop/upload/UploadProjectItem.qml" line="262"/>
        <source>Bad Response</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/Workshop/upload/UploadProjectItem.qml" line="265"/>
        <source>Require Password ReEntry</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/Workshop/upload/UploadProjectItem.qml" line="268"/>
        <source>Value Out Of Range</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/Workshop/upload/UploadProjectItem.qml" line="271"/>
        <source>Unexpecte Error</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/Workshop/upload/UploadProjectItem.qml" line="274"/>
        <source>Disabled</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/Workshop/upload/UploadProjectItem.qml" line="277"/>
        <source>Invalid CEG Submission</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/Workshop/upload/UploadProjectItem.qml" line="280"/>
        <source>Restricted Device</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/Workshop/upload/UploadProjectItem.qml" line="283"/>
        <source>Region Locked</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/Workshop/upload/UploadProjectItem.qml" line="286"/>
        <source>Rate Limit Exceeded</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/Workshop/upload/UploadProjectItem.qml" line="289"/>
        <source>Account Login Denied Need Two Factor</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/Workshop/upload/UploadProjectItem.qml" line="292"/>
        <source>Item Deleted</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/Workshop/upload/UploadProjectItem.qml" line="295"/>
        <source>Account Login Denied Throttle</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/Workshop/upload/UploadProjectItem.qml" line="298"/>
        <source>Two Factor Code Mismatch</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/Workshop/upload/UploadProjectItem.qml" line="301"/>
        <source>Two Factor Activation Code Mismatch</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/Workshop/upload/UploadProjectItem.qml" line="304"/>
        <source>Account Associated To Multiple Partners</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/Workshop/upload/UploadProjectItem.qml" line="307"/>
        <source>Not Modified</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/Workshop/upload/UploadProjectItem.qml" line="310"/>
        <source>No Mobile Device</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/Workshop/upload/UploadProjectItem.qml" line="313"/>
        <source>Time Not Synced</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/Workshop/upload/UploadProjectItem.qml" line="316"/>
        <source>Sms Code Failed</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/Workshop/upload/UploadProjectItem.qml" line="319"/>
        <source>Account Limit Exceeded</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/Workshop/upload/UploadProjectItem.qml" line="322"/>
        <source>Account Activity Limit Exceeded</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/Workshop/upload/UploadProjectItem.qml" line="325"/>
        <source>Phone Activity Limit Exceeded</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/Workshop/upload/UploadProjectItem.qml" line="328"/>
        <source>Refund To Wallet</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/Workshop/upload/UploadProjectItem.qml" line="331"/>
        <source>Email Send Failure</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/Workshop/upload/UploadProjectItem.qml" line="334"/>
        <source>Not Settled</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/Workshop/upload/UploadProjectItem.qml" line="337"/>
        <source>Need Captcha</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/Workshop/upload/UploadProjectItem.qml" line="340"/>
        <source>GSLT Denied</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/Workshop/upload/UploadProjectItem.qml" line="343"/>
        <source>GS Owner Denied</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/Workshop/upload/UploadProjectItem.qml" line="346"/>
        <source>Invalid Item Type</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/Workshop/upload/UploadProjectItem.qml" line="349"/>
        <source>IP Banned</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/Workshop/upload/UploadProjectItem.qml" line="352"/>
        <source>GSLT Expired</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/Workshop/upload/UploadProjectItem.qml" line="355"/>
        <source>Insufficient Funds</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/Workshop/upload/UploadProjectItem.qml" line="358"/>
        <source>Too Many Pending</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/Workshop/upload/UploadProjectItem.qml" line="361"/>
        <source>No Site Licenses Found</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/Workshop/upload/UploadProjectItem.qml" line="364"/>
        <source>WG Network Send Exceeded</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/Workshop/upload/UploadProjectItem.qml" line="367"/>
        <source>Account Not Friends</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/Workshop/upload/UploadProjectItem.qml" line="370"/>
        <source>Limited User Account</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/Workshop/upload/UploadProjectItem.qml" line="373"/>
        <source>Cant Remove Item</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/Workshop/upload/UploadProjectItem.qml" line="376"/>
        <source>Account Deleted</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/Workshop/upload/UploadProjectItem.qml" line="379"/>
        <source>Existing User Cancelled License</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/Workshop/upload/UploadProjectItem.qml" line="382"/>
        <source>Community Cooldown</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/Workshop/upload/UploadProjectItem.qml" line="446"/>
        <source>Status:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/Workshop/upload/UploadProjectItem.qml" line="463"/>
        <source>Upload Progress: </source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>Workshop</name>
    <message>
        <location filename="../qml/Workshop/Workshop.qml" line="133"/>
        <source>Loading</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/Workshop/Workshop.qml" line="144"/>
        <source>Download now!</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/Workshop/Workshop.qml" line="149"/>
        <source>Downloading...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/Workshop/Workshop.qml" line="156"/>
        <source>Details</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/Workshop/Workshop.qml" line="178"/>
        <source>Open In Steam</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/Workshop/Workshop.qml" line="252"/>
        <source>Search for Wallpaper and Widgets...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/Workshop/Workshop.qml" line="288"/>
        <source>Ranked By Vote</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/Workshop/Workshop.qml" line="291"/>
        <source>Publication Date</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/Workshop/Workshop.qml" line="294"/>
        <source>Ranked By Trend</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/Workshop/Workshop.qml" line="297"/>
        <source>Favorited By Friends</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/Workshop/Workshop.qml" line="300"/>
        <source>Created By Friends</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/Workshop/Workshop.qml" line="303"/>
        <source>Created By Followed Users</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/Workshop/Workshop.qml" line="306"/>
        <source>Not Yet Rated</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/Workshop/Workshop.qml" line="309"/>
        <source>Total VotesAsc</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/Workshop/Workshop.qml" line="312"/>
        <source>Votes Up</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/Workshop/Workshop.qml" line="315"/>
        <source>Total Unique Subscriptions</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>WorkshopItem</name>
    <message>
        <location filename="../qml/Workshop/WorkshopItem.qml" line="184"/>
        <source>Download</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/Workshop/WorkshopItem.qml" line="302"/>
        <source>Successfully subscribed to Workshop Item!</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/Workshop/WorkshopItem.qml" line="412"/>
        <source>Download complete!</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>XMLNewsfeed</name>
    <message>
        <location filename="../qml/Community/XMLNewsfeed.qml" line="53"/>
        <source>News &amp; Patchnotes</source>
        <translation>뉴스 &amp; 패치노트</translation>
    </message>
</context>
<context>
    <name>main</name>
    <message>
        <source>ScreenPlay - Double click to change you settings.</source>
        <translation type="vanished">ScreenPlay - 더블 클릭하여 설정을 변경하세요.</translation>
    </message>
    <message>
        <source>Open ScreenPlay</source>
        <translation type="vanished">ScreenPlay 열기</translation>
    </message>
    <message>
        <source>Mute all</source>
        <translation type="vanished">모두 음소거</translation>
    </message>
    <message>
        <source>Unmute all</source>
        <translation type="vanished">모두 음소거 해제</translation>
    </message>
    <message>
        <source>Pause all</source>
        <translation type="vanished">모두 일시정지</translation>
    </message>
    <message>
        <source>Play all</source>
        <translation type="vanished">모두 재생</translation>
    </message>
    <message>
        <source>Quit</source>
        <translation type="vanished">종료</translation>
    </message>
</context>
</TS>
