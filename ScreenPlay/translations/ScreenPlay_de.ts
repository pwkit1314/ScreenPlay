<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="de_DE">
<context>
    <name>Community</name>
    <message>
        <source>We use Stomt because it provides quick and easy feedback via I like/I wish. So you can easily give us feedback and speak your mind. We will read these wishes on a daily basis!</source>
        <translation type="obsolete">Wir verwenden Stomt, weil es ein schnelles und einfaches Feedback über I like/I wish liefert. So kannst du uns einfach Feedback geben und uns deine Meinung sagen. Wir lesen alle Wünsche täglich!</translation>
    </message>
    <message>
        <source>Open ScreenPlay Stomt page</source>
        <translation type="obsolete">ScreenPlay Stomt-Seite öffnen</translation>
    </message>
    <message>
        <source>Forums</source>
        <translation type="obsolete">Forum</translation>
    </message>
    <message>
        <source>Blog</source>
        <translation type="obsolete">Blog</translation>
    </message>
    <message>
        <source>Source Code</source>
        <translation type="obsolete">Source Code</translation>
    </message>
    <message>
        <source>Workshop</source>
        <translation type="obsolete">Workshop</translation>
    </message>
    <message>
        <location filename="../qml/Community/Community.qml" line="34"/>
        <source>Wiki</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/Community/Community.qml" line="40"/>
        <source>Forum</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/Community/Community.qml" line="45"/>
        <source>Issue List</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/Community/Community.qml" line="50"/>
        <source>Release Notes</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/Community/Community.qml" line="55"/>
        <source>Contribution Guide</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/Community/Community.qml" line="60"/>
        <source>Steam Workshop</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>CommunityNavItem</name>
    <message>
        <location filename="../qml/Community/CommunityNavItem.qml" line="59"/>
        <source>Open in browser</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>Create</name>
    <message>
        <location filename="../qml/Create/Create.qml" line="101"/>
        <source>Create wallpapers and widgets for local usage or the steam workshop!</source>
        <translation type="unfinished">Erstelle Wallpaper und Widgets für den eigenen Gebrauch oder den Steam Workshop!</translation>
    </message>
</context>
<context>
    <name>CreateContent</name>
    <message>
        <location filename="../qml/Create/CreateContent.qml" line="15"/>
        <source>Create Widgets and Scenes</source>
        <translation type="unfinished">Erstelle Widgets und Szenen</translation>
    </message>
    <message>
        <source>Create Emtpy Widget</source>
        <translation type="obsolete">Leeres Widget erstellen</translation>
    </message>
    <message>
        <source>Examples Widgets and Scenes</source>
        <translation type="obsolete">Beispiel Widgets und Szenen</translation>
    </message>
    <message>
        <location filename="../qml/Create/CreateContent.qml" line="29"/>
        <source>Create Empty Widget</source>
        <translation type="unfinished">Erstelle ein leeres Widget</translation>
    </message>
    <message>
        <location filename="../qml/Create/CreateContent.qml" line="42"/>
        <source>Example Widgets and Scenes</source>
        <translation type="unfinished">Beispiel Widgets und Szenen</translation>
    </message>
    <message>
        <location filename="../qml/Create/CreateContent.qml" line="65"/>
        <source>Empty HTML Wallpaper</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Simple clock widget</source>
        <translation type="obsolete">Einfaches Uhr-Widget</translation>
    </message>
    <message>
        <location filename="../qml/Create/CreateContent.qml" line="75"/>
        <source>Musik scene wallpaper visualizer</source>
        <translation type="unfinished">Musikszene-Hintergrundvisualisierung</translation>
    </message>
    <message>
        <location filename="../qml/Create/CreateContent.qml" line="86"/>
        <source>Changing scene wallpaper via unsplash.com</source>
        <translation type="unfinished">Ändern von Szenenhintergründen über unsplash.com</translation>
    </message>
</context>
<context>
    <name>CreateContentButton</name>
    <message>
        <location filename="../qml/Create/CreateContentButton.qml" line="114"/>
        <source>Not yet implemented. Stay tuned!</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>CreateEmptyHtmlWallpaper</name>
    <message>
        <location filename="../qml/Create/Wizards/CreateEmptyHtmlWallpaper/CreateEmptyHtmlWallpaper.qml" line="54"/>
        <source>This wizard lets you create a empty html based wallpaper. You can put anything you can imagine into this html file. For example this can be a three.js scene or a utility application written in javascript.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/Create/Wizards/CreateEmptyHtmlWallpaper/CreateEmptyHtmlWallpaper.qml" line="68"/>
        <source>Next</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/Create/Wizards/CreateEmptyHtmlWallpaper/CreateEmptyHtmlWallpaper.qml" line="119"/>
        <source>Create a html Wallpaper</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/Create/Wizards/CreateEmptyHtmlWallpaper/CreateEmptyHtmlWallpaper.qml" line="123"/>
        <source>General</source>
        <translation type="unfinished">Allgemein</translation>
    </message>
    <message>
        <location filename="../qml/Create/Wizards/CreateEmptyHtmlWallpaper/CreateEmptyHtmlWallpaper.qml" line="131"/>
        <source>Wallpaper name</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/Create/Wizards/CreateEmptyHtmlWallpaper/CreateEmptyHtmlWallpaper.qml" line="145"/>
        <source>Copyright owner</source>
        <translation type="unfinished">Urheberrechtsinhaber</translation>
    </message>
    <message>
        <location filename="../qml/Create/Wizards/CreateEmptyHtmlWallpaper/CreateEmptyHtmlWallpaper.qml" line="149"/>
        <source>License</source>
        <translation type="unfinished">Lizenz</translation>
    </message>
    <message>
        <location filename="../qml/Create/Wizards/CreateEmptyHtmlWallpaper/CreateEmptyHtmlWallpaper.qml" line="172"/>
        <source>Tags</source>
        <translation type="unfinished">Schlagwörter</translation>
    </message>
    <message>
        <location filename="../qml/Create/Wizards/CreateEmptyHtmlWallpaper/CreateEmptyHtmlWallpaper.qml" line="184"/>
        <source>Preview Image</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/Create/Wizards/CreateEmptyHtmlWallpaper/CreateEmptyHtmlWallpaper.qml" line="191"/>
        <source>You can set your own preview image here!</source>
        <translation type="unfinished">Hier kannst du dein eigenes Vorschaubild einstellen!</translation>
    </message>
    <message>
        <location filename="../qml/Create/Wizards/CreateEmptyHtmlWallpaper/CreateEmptyHtmlWallpaper.qml" line="216"/>
        <source>Abort</source>
        <translation type="unfinished">Abbrechen</translation>
    </message>
    <message>
        <location filename="../qml/Create/Wizards/CreateEmptyHtmlWallpaper/CreateEmptyHtmlWallpaper.qml" line="228"/>
        <source>Save</source>
        <translation type="unfinished">Speichern</translation>
    </message>
    <message>
        <location filename="../qml/Create/Wizards/CreateEmptyHtmlWallpaper/CreateEmptyHtmlWallpaper.qml" line="263"/>
        <source>Create Html Wallpaper...</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>CreateEmptyWidget</name>
    <message>
        <location filename="../qml/Create/Wizards/CreateEmptyWidget/CreateEmptyWidget.qml" line="17"/>
        <source>Create an empty widget</source>
        <translation type="unfinished">Ein leeres Widget erstellen</translation>
    </message>
    <message>
        <location filename="../qml/Create/Wizards/CreateEmptyWidget/CreateEmptyWidget.qml" line="77"/>
        <source>General</source>
        <translation type="unfinished">Allgemein</translation>
    </message>
    <message>
        <location filename="../qml/Create/Wizards/CreateEmptyWidget/CreateEmptyWidget.qml" line="85"/>
        <source>Widget name</source>
        <translation type="unfinished">Widget Name</translation>
    </message>
    <message>
        <location filename="../qml/Create/Wizards/CreateEmptyWidget/CreateEmptyWidget.qml" line="98"/>
        <source>Copyright owner</source>
        <translation type="unfinished">Urheberrechtsinhaber</translation>
    </message>
    <message>
        <location filename="../qml/Create/Wizards/CreateEmptyWidget/CreateEmptyWidget.qml" line="102"/>
        <source>Type</source>
        <translation type="unfinished">Typ</translation>
    </message>
    <message>
        <location filename="../qml/Create/Wizards/CreateEmptyWidget/CreateEmptyWidget.qml" line="131"/>
        <source>License</source>
        <translation type="unfinished">Lizenz</translation>
    </message>
    <message>
        <location filename="../qml/Create/Wizards/CreateEmptyWidget/CreateEmptyWidget.qml" line="154"/>
        <source>Tags</source>
        <translation type="unfinished">Schlagwörter</translation>
    </message>
    <message>
        <location filename="../qml/Create/Wizards/CreateEmptyWidget/CreateEmptyWidget.qml" line="178"/>
        <source>Save</source>
        <translation type="unfinished">Speichern</translation>
    </message>
    <message>
        <location filename="../qml/Create/Wizards/CreateEmptyWidget/CreateEmptyWidget.qml" line="205"/>
        <source>Abort</source>
        <translation type="unfinished">Abbrechen</translation>
    </message>
    <message>
        <location filename="../qml/Create/Wizards/CreateEmptyWidget/CreateEmptyWidget.qml" line="233"/>
        <source>Create Widget...</source>
        <translation type="unfinished">Widget erstellen...</translation>
    </message>
</context>
<context>
    <name>CreateImport</name>
    <message>
        <location filename="../qml/Create/CreateImport.qml" line="137"/>
        <source></source>
        <translation></translation>
    </message>
</context>
<context>
    <name>CreateUpload</name>
    <message>
        <location filename="../qml/Create/CreateUpload.qml" line="219"/>
        <source></source>
        <translation></translation>
    </message>
</context>
<context>
    <name>CreateWallpaperCodec</name>
    <message>
        <location filename="../qml/Create/Wizards/CreateWallpaper/CreateWallpaperCodec.qml" line="52"/>
        <source>Import a video</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/Create/Wizards/CreateWallpaper/CreateWallpaperCodec.qml" line="62"/>
        <source>Depending on your PC configuration it is better to convert your wallpaper to a specific video codec. If both have bad performance you can also try a QML wallpaper!</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/Create/Wizards/CreateWallpaper/CreateWallpaperCodec.qml" line="76"/>
        <source>Set your preffered video codec:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/Create/Wizards/CreateWallpaper/CreateWallpaperCodec.qml" line="108"/>
        <source>Open Documentation</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/Create/Wizards/CreateWallpaper/CreateWallpaperCodec.qml" line="125"/>
        <source>Next</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>CreateWallpaperResult</name>
    <message>
        <location filename="../qml/Create/Wizards/CreateWallpaper/CreateWallpaperResult.qml" line="18"/>
        <source>An error occurred!</source>
        <translation type="unfinished">Es ist ein Fehler aufgetreten!</translation>
    </message>
    <message>
        <location filename="../qml/Create/Wizards/CreateWallpaper/CreateWallpaperResult.qml" line="78"/>
        <source>Copy text to clipboard</source>
        <translation type="unfinished">Kopiere den Text in die Zwischenablage</translation>
    </message>
    <message>
        <location filename="../qml/Create/Wizards/CreateWallpaper/CreateWallpaperResult.qml" line="88"/>
        <source>Back to create and send an error report!</source>
        <translation type="unfinished">Zurück zum Erstellen und einen Fehlerbericht senden!</translation>
    </message>
</context>
<context>
    <name>CreateWallpaperVideoImportConvert</name>
    <message>
        <location filename="../qml/Create/Wizards/CreateWallpaper/CreateWallpaperVideoImportConvert.qml" line="54"/>
        <source>Generating preview image...</source>
        <translation type="unfinished">Erzeuge Vorschaubild...</translation>
    </message>
    <message>
        <location filename="../qml/Create/Wizards/CreateWallpaper/CreateWallpaperVideoImportConvert.qml" line="57"/>
        <source>Generating preview thumbnail image...</source>
        <translation type="unfinished">Erzeuge Vorschau-Miniaturbild...</translation>
    </message>
    <message>
        <location filename="../qml/Create/Wizards/CreateWallpaper/CreateWallpaperVideoImportConvert.qml" line="64"/>
        <source>Generating 5 second preview video...</source>
        <translation type="unfinished">Generiere ein 5-Sekunden-Vorschau-Video...</translation>
    </message>
    <message>
        <location filename="../qml/Create/Wizards/CreateWallpaper/CreateWallpaperVideoImportConvert.qml" line="67"/>
        <source>Generating preview gif...</source>
        <translation type="unfinished">Generiere Vorschau-Gif...</translation>
    </message>
    <message>
        <location filename="../qml/Create/Wizards/CreateWallpaper/CreateWallpaperVideoImportConvert.qml" line="76"/>
        <source>Converting Audio...</source>
        <translation type="unfinished">Konvertiere Audio...</translation>
    </message>
    <message>
        <location filename="../qml/Create/Wizards/CreateWallpaper/CreateWallpaperVideoImportConvert.qml" line="79"/>
        <source>Converting Video... This can take some time!</source>
        <translation type="unfinished">Video wird umgewandelt... Das kann etwas dauern!</translation>
    </message>
    <message>
        <location filename="../qml/Create/Wizards/CreateWallpaper/CreateWallpaperVideoImportConvert.qml" line="83"/>
        <source>Converting Video ERROR!</source>
        <translation type="unfinished">Konvertieren nicht erfolgreich!</translation>
    </message>
    <message>
        <location filename="../qml/Create/Wizards/CreateWallpaper/CreateWallpaperVideoImportConvert.qml" line="86"/>
        <source>Analyse Video ERROR!</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/Create/Wizards/CreateWallpaper/CreateWallpaperVideoImportConvert.qml" line="114"/>
        <source>Convert a video to a wallpaper</source>
        <translation type="unfinished">Konvertiere ein Video in ein Hintergrund Live Wallpaper</translation>
    </message>
    <message>
        <location filename="../qml/Create/Wizards/CreateWallpaper/CreateWallpaperVideoImportConvert.qml" line="164"/>
        <source></source>
        <translation></translation>
    </message>
    <message>
        <location filename="../qml/Create/Wizards/CreateWallpaper/CreateWallpaperVideoImportConvert.qml" line="211"/>
        <source>Generating preview video...</source>
        <translation type="unfinished">Generiere Vorschau-Video...</translation>
    </message>
    <message>
        <location filename="../qml/Create/Wizards/CreateWallpaper/CreateWallpaperVideoImportConvert.qml" line="224"/>
        <source>You can set your own preview image here!</source>
        <translation type="unfinished">Hier kannst du dein eigenes Vorschaubild einstellen!</translation>
    </message>
    <message>
        <location filename="../qml/Create/Wizards/CreateWallpaper/CreateWallpaperVideoImportConvert.qml" line="258"/>
        <source>Name (required!)</source>
        <translation type="unfinished">Name (erforderlich!)</translation>
    </message>
    <message>
        <location filename="../qml/Create/Wizards/CreateWallpaper/CreateWallpaperVideoImportConvert.qml" line="273"/>
        <source>Description</source>
        <translation type="unfinished">Beschreibung</translation>
    </message>
    <message>
        <location filename="../qml/Create/Wizards/CreateWallpaper/CreateWallpaperVideoImportConvert.qml" line="281"/>
        <source>Youtube URL</source>
        <translation type="unfinished">YouTube-URL</translation>
    </message>
    <message>
        <location filename="../qml/Create/Wizards/CreateWallpaper/CreateWallpaperVideoImportConvert.qml" line="309"/>
        <source>Abort</source>
        <translation type="unfinished">Abbrechen</translation>
    </message>
    <message>
        <location filename="../qml/Create/Wizards/CreateWallpaper/CreateWallpaperVideoImportConvert.qml" line="322"/>
        <source>Save</source>
        <translation type="unfinished">Speichern</translation>
    </message>
    <message>
        <location filename="../qml/Create/Wizards/CreateWallpaper/CreateWallpaperVideoImportConvert.qml" line="359"/>
        <source>Save Wallpaper...</source>
        <translation type="unfinished">Speicher Wallpaper...</translation>
    </message>
</context>
<context>
    <name>DefaultVideoControls</name>
    <message>
        <location filename="../qml/Monitors/DefaultVideoControls.qml" line="41"/>
        <source>Volume</source>
        <translation type="unfinished">Lautstärke</translation>
    </message>
    <message>
        <location filename="../qml/Monitors/DefaultVideoControls.qml" line="55"/>
        <source>Playback rate</source>
        <translation type="unfinished">Wiedergabegeschwindigkeit</translation>
    </message>
    <message>
        <location filename="../qml/Monitors/DefaultVideoControls.qml" line="67"/>
        <source>Current Video Time</source>
        <translation type="unfinished">Aktuelle Videozeit</translation>
    </message>
    <message>
        <location filename="../qml/Monitors/DefaultVideoControls.qml" line="85"/>
        <source>Fill Mode</source>
        <translation type="unfinished">Füll-Modus</translation>
    </message>
    <message>
        <location filename="../qml/Monitors/DefaultVideoControls.qml" line="110"/>
        <source>Stretch</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/Monitors/DefaultVideoControls.qml" line="113"/>
        <source>Fill</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/Monitors/DefaultVideoControls.qml" line="116"/>
        <source>Contain</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/Monitors/DefaultVideoControls.qml" line="119"/>
        <source>Cover</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/Monitors/DefaultVideoControls.qml" line="122"/>
        <source>Scale_Down</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>FFMPEGPopup</name>
    <message>
        <source>Begin downloading FFMPEG</source>
        <translation type="obsolete">Mit dem Herunterladen von FFMPEG beginnen</translation>
    </message>
    <message>
        <source>FFMPEG download failed</source>
        <translation type="obsolete">Der FFMPEG-Download  ist fehlgeschlagen</translation>
    </message>
    <message>
        <source>FFMPEG download successful</source>
        <translation type="obsolete">Der FFMPEG-Download war erfolgreich</translation>
    </message>
    <message>
        <source>Extracting FFMPEG</source>
        <translation type="obsolete">FFMPEG wird extrahiert</translation>
    </message>
    <message>
        <source>ERROR extracting ffmpeg from RAM</source>
        <translation type="obsolete">ERROR beim extrahieren von ffmpeg aus dem RAM</translation>
    </message>
    <message>
        <source>ERROR extracing ffmpeg</source>
        <translation type="obsolete">ERROR beim extrahieren von ffmpeg</translation>
    </message>
    <message>
        <source>ERROR saving FFMPEG to disk</source>
        <translation type="obsolete">ERROR beim speichern von FFMPEG auf der Festplatte</translation>
    </message>
    <message>
        <source>ERROR extracing FFPROBE</source>
        <translation type="obsolete">ERROR beim extrahieren von FFPROBE</translation>
    </message>
    <message>
        <source>ERROR saving FFPROBE to disk</source>
        <translation type="obsolete">ERROR beim speichern von FFPROBE auf der Festplatte</translation>
    </message>
    <message>
        <source>Extraction successful</source>
        <translation type="obsolete">Extrahieren war erfolgreich</translation>
    </message>
    <message>
        <source>All done and ready to go!</source>
        <translation type="obsolete">Alles fertig und einsatzbereit!</translation>
    </message>
    <message>
        <source>You cannot create Wallaper without FFMPEG installed!</source>
        <translation type="obsolete">Ohne installiertes FFMPEG kannst du keine Wallpaper erstellen!</translation>
    </message>
    <message>
        <source>Abort</source>
        <translation type="obsolete">Abbrechen</translation>
    </message>
    <message>
        <source>Download FFMPEG</source>
        <translation type="obsolete">FFMPEG herunterladen</translation>
    </message>
    <message>
        <source>Before we can start creating content you need to download FFMPEG</source>
        <translation type="obsolete">Bevor wir mit der Erstellung von Inhalten beginnen können, musst du FFMPEG herunterladen</translation>
    </message>
    <message>
        <source>&lt;b&gt;Why do we bother you with this?&lt;/b&gt;
&lt;br&gt;&lt;br&gt; Well its because of &lt;b&gt;copyright&lt;/b&gt; and many many &lt;b&gt;patents&lt;/b&gt;.
Files like .mp4 or .webm are containers for video and audio. Every audio
and video file is encoded with a certain codec. These can be open source
ceand free to use like &lt;a href=&apos;https://wikipedia.org/wiki/VP8&apos;&gt;VP8&lt;/a&gt; and the newer  &lt;a href=&apos;https://wikipedia.org/wiki/VP9&apos;&gt;VP9&lt;/a&gt; (the one YouTube uses for their web
ms)but there are also some proprietary ones like  &lt;a href=&apos;https://wikipedia.org/wiki/H.264/MPEG-4_AVC&apos;&gt;h264&lt;/a&gt; and the successor &lt;a href=&apos;https://wikipedia.org/wiki/High_Efficiency_Video_Coding&apos;&gt;h265&lt;/a&gt;.
&lt;br&gt;
&lt;br&gt;


 We as software developer now need to deal with stuff like this in a field wedo not have any expertise in. The desicion to enable only free codecs for content wasan easy one but we still need to provide a way for our user to import wallpaper without a hassle. We do not provide  &lt;a href=&apos;https://ffmpeg.org/&apos;&gt;FFMPEG&lt;/a&gt; for converting video and audio with ScreenPlay because we are not allowed to. We let the user download &lt;a href=&apos;https://ffmpeg.org/&apos;&gt;FFMPEG&lt;/a&gt; wich is perfectly fine!
&lt;br&gt;
Sorry for this little inconvenience :)
&lt;br&gt;
&lt;br&gt;
&lt;center&gt;
&lt;b&gt;
IF YOU DO NOT HAVE A INTERNET CONNECT YOU CAN SIMPLY PUT FFMPEG AND FFPROBE
IN THE SAME FOLDER AS YOUR SCREENPLAY EXECUTABLE!
&lt;/b&gt;
&lt;br&gt;
&lt;br&gt;
This is usually:
&lt;br&gt; C:Program Files (x86)SteamsteamappscommonScreenPlay
&lt;br&gt;
if you installed ScreenPlay via Steam!
&lt;/center&gt;
&lt;br&gt;
&lt;br&gt;
&lt;b&gt;~ Kelteseth | Elias Steurer&lt;/b&gt;</source>
        <translation type="obsolete">&lt;b&gt;Warum belästigen wir dich damit?&lt;/b&gt;
&lt;br&gt;&lt;&lt;br&gt; Es ist wegen &lt;b&gt;Copyright&lt;/b&gt; und vielen vielen &lt;b&gt;Patenten&lt;/b&gt;.
Dateien wie .mp4 oder .webm sind Container für Video und Audio. Jede Audio
und Videodatei ist mit einem bestimmten Codec kodiert. Diese können quelloffen 
und frei zu benutzen sein, wie &lt;a href=&apos;https://wikipedia.org/wiki/VP8&apos;&gt;VP8&lt;/a&gt; und das neuere &lt;a href=&apos;https://wikipedia.org/wiki/VP9&apos;&gt;VP9&lt;/a&gt; (Dasselbe, welches YouTube für ihre Web
ms nutzt) aber es gibt auch einige proprietäre wie &lt;a href=&apos;https://wikipedia.org/wiki/H.264/MPEG-4_AVC&apos;&gt;h264&lt;/a&gt; und den Nachfolger &lt;a href=&apos;https://wikipedia.org/wiki/High_Efficiency_Video_Coding&apos;&gt;h265&lt;/a&gt;.
&lt;br&gt;
&lt;br&gt;


 Wir als Softwareentwickler müssen uns jetzt mit solchen Dingen in einem Bereich beschäftigen, in dem wir keine Expertise haben. Die Entscheidung, nur freie Codecs für Inhalte zu aktivieren, war einfach, aber wir müssen immer noch einen Weg für unsere Nutzer bieten, um Wallpaper ohne Probleme zu importieren. Wir stellen kein &lt;a href=&apos;https://ffmpeg.org/&apos;&gt;FFMPEG&lt;/a&gt; für die Konvertierung von Video und Audio mit ScreenPlay zur Verfügung, weil wir das nicht dürfen. Wir lassen den Nutzer &lt;a href=&apos;https://ffmpeg.org/&apos;&gt;FFMPEG&lt;/a&gt; herunterladen, was völlig in Ordnung ist!
&lt;br&gt;
Entschuldigung für diese kleine Unannehmlichkeit :)
&lt;br&gt;
&lt;br&gt;
&lt;Zentrum&gt;
&lt;b&gt;
WENN SIE KEINE INTERNETVERBINDUNG HABEN, KÖNNEN SIE EINFACH FFMPEG UND FFPROBE
IN DEN GLEICHEN ORDNER WIE IHRE SCREENPLAY.EXE EINFÜGEN!
&lt;/b&gt;
&lt;br&gt;
&lt;br&gt;
Das ist meistens:
&lt;br&gt; C:Programmdateien (x86)SteamsteamappscommonScreenPlay
&lt;br&gt;
wenn du ScreenPlay über Steam installiert hast!
&lt;/center&gt;
&lt;br&gt;
&lt;br&gt;
&lt;b&gt;~ Kelteseth | Elias Steurer&lt;/b&gt;</translation>
    </message>
    <message>
        <source>&lt;b&gt;Why do we bother you with this?&lt;/b&gt;
&lt;br&gt;&lt;br&gt; Well its because of &lt;b&gt;copyright&lt;/b&gt; and many many &lt;b&gt;patents&lt;/b&gt;.
Files like .mp4 or .webm are containers for video and audio. Every audio
and video file is encoded with a certain codec. These can be open sour
ceand free to use like &lt;a href=&apos;https://wikipedia.org/wiki/VP8&apos;&gt;VP8&lt;/a&gt; and the newer  &lt;a href=&apos;https://wikipedia.org/wiki/VP9&apos;&gt;VP9&lt;/a&gt; (the one YouTube uses for their web
ms)but there are also some proproatary ones like  &lt;a href=&apos;https://wikipedia.org/wiki/H.264/MPEG-4_AVC&apos;&gt;h264&lt;/a&gt; and the successor &lt;a href=&apos;https://wikipedia.org/wiki/High_Efficiency_Video_Coding&apos;&gt;h265&lt;/a&gt;.
&lt;br&gt;
&lt;br&gt;


 We as software developer now need to deal with stuff like this in a field we
are do not have any expertise in. The desicion to enable only free codecs for content was
an easy one but we still need to provide a way for our user to import wallpape
r without a hassle. We do not provide  &lt;a href=&apos;https://ffmpeg.org/&apos;&gt;FFMPEG&lt;/a&gt; f
or converting video and audio with ScreenPlay because we are not allowed to. We let the user download &lt;a href=&apos;https://ffmpeg.org/&apos;&gt;FFMPEG&lt;/a&gt; wich is perfectly fine!
&lt;br&gt;
Sorry for this little inconvenience :)
&lt;br&gt;
&lt;br&gt;
&lt;center&gt;
&lt;b&gt;
IF YOU DO NOT HAVE A INTERNET CONNECT YOU CAN SIMPLY PUT FFMPEG AND FFPROBE
IN THE SAME FOLDER AS YOUR SCREENPLAY EXECUTABLE!
&lt;/b&gt;
&lt;br&gt;
&lt;br&gt;
This is usually:
&lt;br&gt; C:Program Files (x86)SteamsteamappscommonScreenPlay
&lt;br&gt;
if you installed ScreenPlay via Steam!
&lt;/center&gt;
&lt;br&gt;
&lt;br&gt;
&lt;b&gt;~ Kelteseth | Elias Steurer&lt;/b&gt;</source>
        <translation type="obsolete">&lt;b&gt;Warum nerfen wir Sie damit?&lt;/b&gt;
&lt;br&gt;&lt;&lt;br&gt; Wegen &lt;b&gt;Copyright&lt;/b&gt; und vielen vielen &lt;b&gt;Patenten&lt;/b&gt;.
Dateien wie .mp4 oder .webm sind Container für Video und Audio. Jede Audio
und Videodatei ist mit einem bestimmten Codec kodiert. Diese können Open Source sein
nicht mehr und frei zu benutzen wie &lt;a href=&apos;https://wikipedia.org/wiki/VP8&apos;&gt;VP8&lt;/a&gt; und das neuere &lt;a href=&apos;https://wikipedia.org/wiki/VP9&apos;&gt;VP9&lt;/a&gt; (das, was YouTube für ihre Web
ms)aber es gibt auch einige proproatäre wie &lt;a href=&apos;https://wikipedia.org/wiki/H.264/MPEG-4_AVC&apos;&gt;h264&lt;/a&gt; und den Nachfolger &lt;a href=&apos;https://wikipedia.org/wiki/High_Efficiency_Video_Coding&apos;&gt;h265&lt;/a&gt;.
&lt;br&gt;
&lt;br&gt;


 Wir als Softwareentwickler müssen uns jetzt mit solchen Dingen in einem Bereich beschäftigen, den wir
haben keine Erfahrung in. Die Entscheidung, nur freie Codecs für Inhalte zu aktivieren, wurde
eine einfache, aber wir müssen immer noch einen Weg für unseren Benutzer bieten, Wallpape zu importieren
r ohne Schwierigkeiten. Wir bieten nicht &lt;a href=&apos;https://ffmpeg.org/&apos;&gt;FFMPEG&lt;/a&gt; f
oder die Konvertierung von Video und Audio mit ScreenPlay, weil wir das nicht dürfen. Wir lassen den Benutzer &lt;a href=&apos;https://ffmpeg.org/&apos;&gt;FFMPEG&lt;/a&gt; herunterladen, was völlig in Ordnung ist!
&lt;br&gt;
Entschuldigung für diese kleine Unannehmlichkeit :)
&lt;br&gt;
&lt;br&gt;
&lt;Zentrum&gt;
&lt;b&gt;
WENN SIE KEINE INTERNETVERBINDUNG HABEN, KÖNNEN SIE EINFACH FFMPEG UND FFPROBE
IM GLEICHEN ORDNER WIE IHR AUSFÜHRBARES DREHBUCH!
&lt;/b&gt;
&lt;br&gt;
&lt;br&gt;
Das ist meistens so:
&lt;br&gt; C:Programmdateien (x86)SteamsteamappscommonScreenPlay
&lt;br&gt;
wenn Sie ScreenPlay über Steam! installiert haben
&lt;/center&gt;
&lt;br&gt;
&lt;br&gt;
&lt;b&gt;~ Kelteseth | Elias Steurer&lt;/b&gt;</translation>
    </message>
    <message>
        <source>Not now!</source>
        <translation type="obsolete">Nicht jetzt!</translation>
    </message>
    <message>
        <source>You can now start creating content!</source>
        <translation type="obsolete">Du  kannst jetzt mit der Erstellung von Inhalten beginnen!</translation>
    </message>
    <message>
        <source>Start!</source>
        <translation type="obsolete">Start!</translation>
    </message>
</context>
<context>
    <name>Footer</name>
    <message>
        <location filename="../qml/Create/Footer.qml" line="23"/>
        <source>QML Quickstart Guide</source>
        <translation type="unfinished">QML-Kurzanleitung</translation>
    </message>
    <message>
        <location filename="../qml/Create/Footer.qml" line="34"/>
        <source>Documentation</source>
        <translation type="unfinished">Dokumentation</translation>
    </message>
    <message>
        <location filename="../qml/Create/Footer.qml" line="45"/>
        <source>Forums</source>
        <translation type="unfinished">Forum</translation>
    </message>
    <message>
        <location filename="../qml/Create/Footer.qml" line="56"/>
        <source>Workshop</source>
        <translation type="unfinished">Workshop</translation>
    </message>
</context>
<context>
    <name>Headline</name>
    <message>
        <location filename="../qml/Common/Headline.qml" line="16"/>
        <source>Headline</source>
        <translation type="unfinished">Überschrift</translation>
    </message>
</context>
<context>
    <name>ImageSelector</name>
    <message>
        <location filename="../qml/Common/ImageSelector.qml" line="142"/>
        <source>Clear</source>
        <translation type="unfinished">Leeren</translation>
    </message>
    <message>
        <location filename="../qml/Common/ImageSelector.qml" line="160"/>
        <source>Select Preview Image</source>
        <translation type="unfinished">Wähle ein Vorschaubild</translation>
    </message>
</context>
<context>
    <name>ImportContent</name>
    <message>
        <location filename="../qml/Create/ImportContent.qml" line="22"/>
        <source>Import Content</source>
        <translation type="unfinished">Importiere neue Inhalte</translation>
    </message>
    <message>
        <location filename="../qml/Create/ImportContent.qml" line="75"/>
        <source>Import video</source>
        <translation type="unfinished">Importiere ein Video</translation>
    </message>
    <message>
        <source>FFMPEG Needed for import</source>
        <translation type="obsolete">FFMPEG ist erforderlich fürs Importieren</translation>
    </message>
    <message>
        <source>Import ThreeJs Scene</source>
        <translation type="obsolete">ThreeJs Szene importieren</translation>
    </message>
    <message>
        <location filename="../qml/Create/ImportContent.qml" line="161"/>
        <source>Upload Exsisting Project to Steam</source>
        <translation type="unfinished">Bestehendes Projekt auf Steam hochladen</translation>
    </message>
</context>
<context>
    <name>Installed</name>
    <message>
        <location filename="../qml/Installed/Installed.qml" line="99"/>
        <source>Refreshing!</source>
        <translation type="unfinished">Aktualisiere!</translation>
    </message>
    <message>
        <location filename="../qml/Installed/Installed.qml" line="102"/>
        <location filename="../qml/Installed/Installed.qml" line="124"/>
        <source>Pull to refresh!</source>
        <translation type="unfinished">Drücken zum aktualisieren!</translation>
    </message>
    <message>
        <location filename="../qml/Installed/Installed.qml" line="141"/>
        <source>Get more Wallpaper &amp; Widgets via the Steam workshop!</source>
        <translation type="unfinished">Holen dir mehr Wallpaper und Widgets über den Steam-Workshop!</translation>
    </message>
    <message>
        <location filename="../qml/Installed/Installed.qml" line="217"/>
        <source>Open containing folder</source>
        <translation type="unfinished">Enthaltenden Ordner öffnen</translation>
    </message>
    <message>
        <location filename="../qml/Installed/Installed.qml" line="225"/>
        <source>Deinstall Item</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/Installed/Installed.qml" line="234"/>
        <source>Open workshop Page</source>
        <translation type="unfinished">Workshop-Seite öffnen</translation>
    </message>
    <message>
        <location filename="../qml/Installed/Installed.qml" line="245"/>
        <source>Are you sure you want to delete this item?</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>All</source>
        <translation type="obsolete">Alles</translation>
    </message>
    <message>
        <source>Videos</source>
        <translation type="obsolete">Videos</translation>
    </message>
    <message>
        <source>Scenes</source>
        <translation type="obsolete">Szenen</translation>
    </message>
    <message>
        <source>Widgets</source>
        <translation type="obsolete">Widgets</translation>
    </message>
    <message>
        <location filename="../qml/Installed/Installed.qml" line="329"/>
        <source></source>
        <translation></translation>
    </message>
    <message>
        <source>Search for Wallpaper &amp; Widgets</source>
        <translation type="obsolete">Suche nach Wallpaper &amp; Widgets</translation>
    </message>
</context>
<context>
    <name>InstalledWelcomeScreen</name>
    <message>
        <location filename="../qml/Installed/InstalledWelcomeScreen.qml" line="54"/>
        <source>Get free Widgets and Wallpaper via the Steam Workshop</source>
        <translation type="unfinished">Hole dir kostenlose Widgets und Wallpaper via Steam</translation>
    </message>
    <message>
        <location filename="../qml/Installed/InstalledWelcomeScreen.qml" line="84"/>
        <source>Browse the Steam Workshop</source>
        <translation type="unfinished">Stöbere durch den Steam Workshop</translation>
    </message>
</context>
<context>
    <name>Monitors</name>
    <message>
        <location filename="../qml/Monitors/Monitors.qml" line="67"/>
        <source>Wallpaper Configuration</source>
        <translation type="unfinished">Wallpaper Konfiguration</translation>
    </message>
    <message>
        <location filename="../qml/Monitors/Monitors.qml" line="132"/>
        <source>Remove selected</source>
        <translation type="unfinished">Die Auswahl entfernen</translation>
    </message>
    <message>
        <location filename="../qml/Monitors/Monitors.qml" line="144"/>
        <location filename="../qml/Monitors/Monitors.qml" line="158"/>
        <source>Remove </source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/Monitors/Monitors.qml" line="145"/>
        <source>Wallpapers</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/Monitors/Monitors.qml" line="159"/>
        <source>Widgets</source>
        <translation type="unfinished">Widgets</translation>
    </message>
    <message>
        <source>Remove all Wallpapers</source>
        <translation type="obsolete">Alle Wallpaper entfernen</translation>
    </message>
    <message>
        <source>Remove all Widgets</source>
        <translation type="obsolete">Alle Widgets entfernen</translation>
    </message>
</context>
<context>
    <name>MonitorsProjectSettingItem</name>
    <message>
        <location filename="../qml/Monitors/MonitorsProjectSettingItem.qml" line="132"/>
        <source>Set color</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/Monitors/MonitorsProjectSettingItem.qml" line="155"/>
        <source>Please choose a color</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>Navigation</name>
    <message>
        <location filename="../qml/Installed/Navigation.qml" line="57"/>
        <source>All</source>
        <translation type="unfinished">Alles</translation>
    </message>
    <message>
        <location filename="../qml/Installed/Navigation.qml" line="73"/>
        <source>Scenes</source>
        <translation type="unfinished">Szenen</translation>
    </message>
    <message>
        <location filename="../qml/Installed/Navigation.qml" line="89"/>
        <source>Videos</source>
        <translation type="unfinished">Videos</translation>
    </message>
    <message>
        <location filename="../qml/Installed/Navigation.qml" line="105"/>
        <source>Widgets</source>
        <translation type="unfinished">Widgets</translation>
    </message>
    <message>
        <location filename="../qml/Workshop/Navigation.qml" line="46"/>
        <source> Subscribed items: </source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/Workshop/Navigation.qml" line="87"/>
        <source>Upload to the Steam Workshop</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>NavigationWallpaperConfiguration</name>
    <message>
        <location filename="../qml/Navigation/NavigationWallpaperConfiguration.qml" line="70"/>
        <source>Configurate active Wallpaper or Widgets</source>
        <translation type="unfinished">Konfiguriere aktive Wallpaper oder Widgets</translation>
    </message>
    <message>
        <location filename="../qml/Navigation/NavigationWallpaperConfiguration.qml" line="72"/>
        <source>No active Wallpaper or Widgets</source>
        <translation type="unfinished">Keine aktiven Wallpaper oder Widgets</translation>
    </message>
</context>
<context>
    <name>PopupOffline</name>
    <message>
        <location filename="../qml/Workshop/PopupOffline.qml" line="28"/>
        <source>You need to run Steam for this :)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/Workshop/PopupOffline.qml" line="37"/>
        <source>Back</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>PopupSteamWorkshopAgreement</name>
    <message>
        <location filename="../qml/Workshop/upload/PopupSteamWorkshopAgreement.qml" line="22"/>
        <source>Abort Upload.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/Workshop/upload/PopupSteamWorkshopAgreement.qml" line="30"/>
        <source>I Agree to the Steam Workshop Agreement</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>SaveNotification</name>
    <message>
        <location filename="../qml/Monitors/SaveNotification.qml" line="40"/>
        <source>Profile saved successfully!</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>ScreenPlayItem</name>
    <message>
        <source>Open containing folder</source>
        <translation type="obsolete">Enthaltenden Ordner öffnen</translation>
    </message>
    <message>
        <source>Open workshop Page</source>
        <translation type="obsolete">Workshop-Seite öffnen</translation>
    </message>
</context>
<context>
    <name>Search</name>
    <message>
        <location filename="../qml/Common/Search.qml" line="45"/>
        <source>Search for Wallpaper &amp; Widgets</source>
        <translation type="unfinished">Suche nach Wallpaper &amp; Widgets</translation>
    </message>
</context>
<context>
    <name>Settings</name>
    <message>
        <location filename="../qml/Settings/Settings.qml" line="59"/>
        <source>General</source>
        <translation type="unfinished">Allgemein</translation>
    </message>
    <message>
        <location filename="../qml/Settings/Settings.qml" line="74"/>
        <source>Autostart</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/Settings/Settings.qml" line="75"/>
        <source>ScreenPlay will start with Windows and will setup your Desktop every time for you.</source>
        <translation type="unfinished">ScreenPlay startet mit Windows und richtet deinen Desktop jedes Mal für dich ein.</translation>
    </message>
    <message>
        <location filename="../qml/Settings/Settings.qml" line="83"/>
        <source>High priority Autostart</source>
        <translation type="unfinished">Hohe Priorität für Autostart</translation>
    </message>
    <message>
        <location filename="../qml/Settings/Settings.qml" line="86"/>
        <source>This options grants ScreenPlay a higher autostart priority than other apps.</source>
        <translation type="unfinished">Diese Option gewährt ScreenPlay eine höhere Autostartpriorität als anderen Anwendungen.</translation>
    </message>
    <message>
        <location filename="../qml/Settings/Settings.qml" line="95"/>
        <source>Send anonymous crash reports and statistics</source>
        <translation type="unfinished">Sende anonyme Absturzberichte und Statistiken</translation>
    </message>
    <message>
        <location filename="../qml/Settings/Settings.qml" line="96"/>
        <source>Help us make ScreenPlay faster and more stable. All collected data is purely anonymous and only used for development purposes!</source>
        <translation type="unfinished">Hilf uns, ScreenPlay schneller und stabiler zu machen. Alle gesammelten Daten sind rein anonym und werden nur zu Entwicklungszwecken verwendet!</translation>
    </message>
    <message>
        <location filename="../qml/Settings/Settings.qml" line="105"/>
        <source>Set save location</source>
        <translation type="unfinished">Speicherort auswählen</translation>
    </message>
    <message>
        <location filename="../qml/Settings/Settings.qml" line="110"/>
        <source>Your storage path is empty!</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/Settings/Settings.qml" line="116"/>
        <source>Set location</source>
        <translation type="unfinished">Standort auswählen</translation>
    </message>
    <message>
        <location filename="../qml/Settings/Settings.qml" line="131"/>
        <source>Important: Changing this directory has no effect on the workshop download path. ScreenPlay only supports having one content folder!</source>
        <translation type="unfinished">Wichtig: Eine Änderung dieses Verzeichnisses hat keine Auswirkungen auf den Download-Pfad des Workshops. ScreenPlay unterstützt nur ein Verzeichnis!</translation>
    </message>
    <message>
        <location filename="../qml/Settings/Settings.qml" line="149"/>
        <source>Language</source>
        <translation type="unfinished">Sprache</translation>
    </message>
    <message>
        <location filename="../qml/Settings/Settings.qml" line="150"/>
        <source>Set the ScreenPlay UI Language</source>
        <translation type="unfinished">Wähle die Sprache des Programms aus</translation>
    </message>
    <message>
        <location filename="../qml/Settings/Settings.qml" line="165"/>
        <source>English</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/Settings/Settings.qml" line="168"/>
        <source>German</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/Settings/Settings.qml" line="174"/>
        <source>Russian</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/Settings/Settings.qml" line="177"/>
        <source>French</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/Settings/Settings.qml" line="180"/>
        <source>Spanish</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/Settings/Settings.qml" line="183"/>
        <source>Korean</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/Settings/Settings.qml" line="186"/>
        <source>Vietnamese</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/Settings/Settings.qml" line="194"/>
        <source>Theme</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/Settings/Settings.qml" line="195"/>
        <source>Switch dark/light theme</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/Settings/Settings.qml" line="209"/>
        <source>System Default</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/Settings/Settings.qml" line="212"/>
        <source>Dark</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/Settings/Settings.qml" line="215"/>
        <source>Light</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/Settings/Settings.qml" line="225"/>
        <source>Performance</source>
        <translation type="unfinished">Leistung</translation>
    </message>
    <message>
        <location filename="../qml/Settings/Settings.qml" line="240"/>
        <source>Pause wallpaper video rendering while another app is in the foreground</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/Settings/Settings.qml" line="241"/>
        <source>We disable the video rendering (not the audio!) for the best performance. If you have problem you can disable this behaviour here. Wallpaper restart required!</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Pause wallpaper while ingame</source>
        <translation type="obsolete">Pausiere dein Wallpaper während des Spiels</translation>
    </message>
    <message>
        <source>To maximise your framerates ingame, you can enable this setting to pause all active wallpapers!</source>
        <translation type="obsolete">Um deine Frameraten im Spiel zu maximieren, kannst du diese Einstellung aktivieren, um alle aktiven Wallpaper zu pausieren!</translation>
    </message>
    <message>
        <location filename="../qml/Settings/Settings.qml" line="171"/>
        <source>Chinese - Simplified</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/Settings/Settings.qml" line="251"/>
        <source>Default Fill Mode</source>
        <translation type="unfinished">Standard-Füllmodus</translation>
    </message>
    <message>
        <location filename="../qml/Settings/Settings.qml" line="252"/>
        <source>Set this property to define how the video is scaled to fit the target area.</source>
        <translation type="unfinished">Lege diese Eigenschaft fest, um zu definieren, wie das Video skaliert wird, damit es in den Zielbereich passt.</translation>
    </message>
    <message>
        <location filename="../qml/Settings/Settings.qml" line="264"/>
        <source>Stretch</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/Settings/Settings.qml" line="267"/>
        <source>Fill</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/Settings/Settings.qml" line="270"/>
        <source>Contain</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/Settings/Settings.qml" line="273"/>
        <source>Cover</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/Settings/Settings.qml" line="276"/>
        <source>Scale-Down</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/Settings/Settings.qml" line="286"/>
        <source>About</source>
        <translation type="unfinished">Über</translation>
    </message>
    <message>
        <location filename="../qml/Settings/Settings.qml" line="314"/>
        <source>Thank you for using ScreenPlay</source>
        <translation type="unfinished">Danke, dass du ScreenPlay verwendest</translation>
    </message>
    <message>
        <location filename="../qml/Settings/Settings.qml" line="329"/>
        <source>Hi, I&apos;m Elias Steurer also known as Kelteseth and I&apos;m the developer of ScreenPlay. Thank you for using my software. You can follow me to receive updates about ScreenPlay here:</source>
        <translation type="unfinished">Moin, ich bin Elias Steurer, auch bekannt als Kelteseth und ich bin der Entwickler von ScreenPlay. Danke, dass du meine Software nutzt. Du kannst mir hier folgen, um Updates über ScreenPlay zu erhalten:</translation>
    </message>
    <message>
        <location filename="../qml/Settings/Settings.qml" line="418"/>
        <source>Version</source>
        <translation type="unfinished">Version</translation>
    </message>
    <message>
        <location filename="../qml/Settings/Settings.qml" line="419"/>
        <source>ScreenPlay Build Version </source>
        <translation type="unfinished">ScreenPlay-Build-Version </translation>
    </message>
    <message>
        <location filename="../qml/Settings/Settings.qml" line="421"/>
        <source>Open Changelog</source>
        <translation type="unfinished">Changelog öffnen</translation>
    </message>
    <message>
        <location filename="../qml/Settings/Settings.qml" line="428"/>
        <source>Third Party Software</source>
        <translation type="unfinished">Software von Drittanbietern</translation>
    </message>
    <message>
        <location filename="../qml/Settings/Settings.qml" line="429"/>
        <source>ScreenPlay would not be possible without the work of others. A big thank you to: </source>
        <translation type="unfinished">ScreenPlay wäre ohne die Arbeit anderer nicht möglich. Ein großes Dankeschön dafür geht an: </translation>
    </message>
    <message>
        <location filename="../qml/Settings/Settings.qml" line="430"/>
        <source>Licenses</source>
        <translation type="unfinished">Lizenzen</translation>
    </message>
    <message>
        <location filename="../qml/Settings/Settings.qml" line="448"/>
        <location filename="../qml/Settings/Settings.qml" line="450"/>
        <source>Debug Messages</source>
        <translation type="unfinished">Debugging-Meldungen</translation>
    </message>
    <message>
        <location filename="../qml/Settings/Settings.qml" line="449"/>
        <source>If your ScreenPlay missbehaves this is a good way to look for answers. This shows all logs and warning during runtime.</source>
        <translation type="unfinished">Wenn den ScreenPlay sich falsch verhält, ist hier eine gute Möglichkeit, nach Antworten zu suchen. Hier werden alle Protokolle und Warnungen während der Laufzeit angezeigt.</translation>
    </message>
    <message>
        <location filename="../qml/Settings/Settings.qml" line="461"/>
        <source>Data Protection</source>
        <translation type="unfinished">Datenschutz</translation>
    </message>
    <message>
        <location filename="../qml/Settings/Settings.qml" line="462"/>
        <source>We use you data very carefully to improve ScreenPlay. We do not sell or share this (anonymous) information with others!</source>
        <translation type="unfinished">Wir verwenden deine Daten sehr sorgfältig, um ScreenPlay zu verbessern. Wir verkaufen oder teilen diese (anonymen) Informationen nicht mit anderen!</translation>
    </message>
    <message>
        <location filename="../qml/Settings/Settings.qml" line="463"/>
        <source>Privacy</source>
        <translation type="unfinished">Datenschutz</translation>
    </message>
</context>
<context>
    <name>SettingsExpander</name>
    <message>
        <location filename="../qml/Settings/SettingsExpander.qml" line="51"/>
        <source>Copy text to clipboard</source>
        <translation type="unfinished">Kopiere den Text in die Zwischenablage</translation>
    </message>
</context>
<context>
    <name>Sidebar</name>
    <message>
        <location filename="../qml/Installed/Sidebar.qml" line="99"/>
        <source>Set Widget</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/Installed/Sidebar.qml" line="208"/>
        <source>Headline</source>
        <translation type="unfinished">Überschrift</translation>
    </message>
    <message>
        <location filename="../qml/Installed/Sidebar.qml" line="259"/>
        <source>Select a Monitor to display the content</source>
        <translation type="unfinished">Wähle einen Monitor zur Anzeige des Inhalts</translation>
    </message>
    <message>
        <location filename="../qml/Installed/Sidebar.qml" line="294"/>
        <source>Set Volume</source>
        <translation type="unfinished">Audiolautstärke einstellen</translation>
    </message>
    <message>
        <location filename="../qml/Installed/Sidebar.qml" line="304"/>
        <source>Fill Mode</source>
        <translation type="unfinished">Füll-Modus</translation>
    </message>
    <message>
        <location filename="../qml/Installed/Sidebar.qml" line="327"/>
        <source>Stretch</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/Installed/Sidebar.qml" line="330"/>
        <source>Fill</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/Installed/Sidebar.qml" line="333"/>
        <source>Contain</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/Installed/Sidebar.qml" line="336"/>
        <source>Cover</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/Installed/Sidebar.qml" line="339"/>
        <source>Scale-Down</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Set wallpaper</source>
        <translation type="obsolete">Wallpaper einstellen</translation>
    </message>
    <message>
        <location filename="../qml/Installed/Sidebar.qml" line="96"/>
        <source>Set Wallpaper</source>
        <translation type="unfinished">Wallpaper einstellen</translation>
    </message>
    <message>
        <source>Create Widget</source>
        <translation type="obsolete">Widget erstellen</translation>
    </message>
    <message>
        <source>Create Wallpaper</source>
        <translation type="obsolete">Wallpaper erstellen</translation>
    </message>
    <message>
        <location filename="../qml/Workshop/Sidebar.qml" line="91"/>
        <source>Project size: </source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/Workshop/Sidebar.qml" line="91"/>
        <source> MB</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/Workshop/Sidebar.qml" line="97"/>
        <source>No description...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/Workshop/Sidebar.qml" line="239"/>
        <source>Click here if you like the content</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/Workshop/Sidebar.qml" line="253"/>
        <source>Click here if you do not like the content</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/Workshop/Sidebar.qml" line="288"/>
        <source>Tags: </source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/Workshop/Sidebar.qml" line="313"/>
        <source>Subscribtions: </source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/Workshop/Sidebar.qml" line="319"/>
        <source>Open In Steam</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/Workshop/Sidebar.qml" line="362"/>
        <source>Subscribed!</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/Workshop/Sidebar.qml" line="362"/>
        <source>Subscribe</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>TagSelector</name>
    <message>
        <location filename="../qml/Common/TagSelector.qml" line="12"/>
        <source>Save</source>
        <translation type="unfinished">Speichern</translation>
    </message>
    <message>
        <location filename="../qml/Common/TagSelector.qml" line="15"/>
        <source>Add tag</source>
        <translation type="unfinished">Tag hinzufügen</translation>
    </message>
    <message>
        <location filename="../qml/Common/TagSelector.qml" line="109"/>
        <source>Cancel</source>
        <translation type="unfinished">Abbrechen</translation>
    </message>
    <message>
        <location filename="../qml/Common/TagSelector.qml" line="128"/>
        <source>Add Tag</source>
        <translation type="unfinished">Tag hinzufügen</translation>
    </message>
</context>
<context>
    <name>TrayIcon</name>
    <message>
        <location filename="../qml/Common/TrayIcon.qml" line="9"/>
        <source>ScreenPlay - Double click to change you settings.</source>
        <translation type="unfinished">ScreenPlay - Doppelklicke, um deine Einstellungen zu ändern.</translation>
    </message>
    <message>
        <location filename="../qml/Common/TrayIcon.qml" line="28"/>
        <source>Open ScreenPlay</source>
        <translation type="unfinished">Öffne ScreenPlay</translation>
    </message>
    <message>
        <location filename="../qml/Common/TrayIcon.qml" line="36"/>
        <location filename="../qml/Common/TrayIcon.qml" line="40"/>
        <source>Mute all</source>
        <translation type="unfinished">Alles stummschalten</translation>
    </message>
    <message>
        <location filename="../qml/Common/TrayIcon.qml" line="45"/>
        <source>Unmute all</source>
        <translation type="unfinished">Alle Stummschaltungen aufheben</translation>
    </message>
    <message>
        <location filename="../qml/Common/TrayIcon.qml" line="54"/>
        <location filename="../qml/Common/TrayIcon.qml" line="58"/>
        <source>Pause all</source>
        <translation type="unfinished">Alles pausieren</translation>
    </message>
    <message>
        <location filename="../qml/Common/TrayIcon.qml" line="63"/>
        <source>Play all</source>
        <translation type="unfinished">Alles abspielen</translation>
    </message>
    <message>
        <location filename="../qml/Common/TrayIcon.qml" line="70"/>
        <source>Quit</source>
        <translation type="unfinished">Beenden</translation>
    </message>
</context>
<context>
    <name>UploadProject</name>
    <message>
        <location filename="../qml/Workshop/upload/UploadProject.qml" line="63"/>
        <source>Upload Wallpaper/Widgets to Steam</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/Workshop/upload/UploadProject.qml" line="137"/>
        <source>Abort</source>
        <translation type="unfinished">Abbrechen</translation>
    </message>
    <message>
        <location filename="../qml/Workshop/upload/UploadProject.qml" line="152"/>
        <source>Upload Projects</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/Workshop/upload/UploadProject.qml" line="212"/>
        <source>Finish</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>UploadProjectBigItem</name>
    <message>
        <location filename="../qml/Workshop/upload/UploadProjectBigItem.qml" line="114"/>
        <source>Type: </source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/Workshop/upload/UploadProjectBigItem.qml" line="121"/>
        <source>Open Folder</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/Workshop/upload/UploadProjectBigItem.qml" line="141"/>
        <source>Invalid Project!</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>UploadProjectItem</name>
    <message>
        <location filename="../qml/Workshop/upload/UploadProjectItem.qml" line="43"/>
        <source>Fail</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/Workshop/upload/UploadProjectItem.qml" line="46"/>
        <source>No Connection</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/Workshop/upload/UploadProjectItem.qml" line="49"/>
        <source>Invalid Password</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/Workshop/upload/UploadProjectItem.qml" line="52"/>
        <source>Logged In Elsewhere</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/Workshop/upload/UploadProjectItem.qml" line="55"/>
        <source>Invalid Protocol Version</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/Workshop/upload/UploadProjectItem.qml" line="58"/>
        <source>Invalid Param</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/Workshop/upload/UploadProjectItem.qml" line="61"/>
        <source>File Not Found</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/Workshop/upload/UploadProjectItem.qml" line="64"/>
        <source>Busy</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/Workshop/upload/UploadProjectItem.qml" line="67"/>
        <source>Invalid State</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/Workshop/upload/UploadProjectItem.qml" line="70"/>
        <source>Invalid Name</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/Workshop/upload/UploadProjectItem.qml" line="73"/>
        <source>Invalid Email</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/Workshop/upload/UploadProjectItem.qml" line="76"/>
        <source>Duplicate Name</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/Workshop/upload/UploadProjectItem.qml" line="79"/>
        <source>Access Denied</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/Workshop/upload/UploadProjectItem.qml" line="82"/>
        <source>Timeout</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/Workshop/upload/UploadProjectItem.qml" line="85"/>
        <source>Banned</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/Workshop/upload/UploadProjectItem.qml" line="88"/>
        <source>Account Not Found</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/Workshop/upload/UploadProjectItem.qml" line="91"/>
        <source>Invalid SteamID</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/Workshop/upload/UploadProjectItem.qml" line="94"/>
        <source>Service Unavailable</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/Workshop/upload/UploadProjectItem.qml" line="97"/>
        <source>Not Logged On</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/Workshop/upload/UploadProjectItem.qml" line="100"/>
        <source>Pending</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/Workshop/upload/UploadProjectItem.qml" line="103"/>
        <source>Encryption Failure</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/Workshop/upload/UploadProjectItem.qml" line="106"/>
        <source>Insufficient Privilege</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/Workshop/upload/UploadProjectItem.qml" line="109"/>
        <source>Limit Exceeded</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/Workshop/upload/UploadProjectItem.qml" line="112"/>
        <source>Revoked</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/Workshop/upload/UploadProjectItem.qml" line="115"/>
        <source>Expired</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/Workshop/upload/UploadProjectItem.qml" line="118"/>
        <source>Already Redeemed</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/Workshop/upload/UploadProjectItem.qml" line="121"/>
        <source>Duplicate Request</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/Workshop/upload/UploadProjectItem.qml" line="124"/>
        <source>Already Owned</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/Workshop/upload/UploadProjectItem.qml" line="127"/>
        <source>IP Not Found</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/Workshop/upload/UploadProjectItem.qml" line="130"/>
        <source>Persist Failed</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/Workshop/upload/UploadProjectItem.qml" line="133"/>
        <source>Locking Failed</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/Workshop/upload/UploadProjectItem.qml" line="136"/>
        <source>Logon Session Replaced</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/Workshop/upload/UploadProjectItem.qml" line="139"/>
        <source>Connect Failed</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/Workshop/upload/UploadProjectItem.qml" line="142"/>
        <source>Handshake Failed</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/Workshop/upload/UploadProjectItem.qml" line="145"/>
        <source>IO Failure</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/Workshop/upload/UploadProjectItem.qml" line="148"/>
        <source>Remote Disconnect</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/Workshop/upload/UploadProjectItem.qml" line="151"/>
        <source>Shopping Cart Not Found</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/Workshop/upload/UploadProjectItem.qml" line="154"/>
        <source>Blocked</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/Workshop/upload/UploadProjectItem.qml" line="157"/>
        <source>Ignored</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/Workshop/upload/UploadProjectItem.qml" line="160"/>
        <source>No Match</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/Workshop/upload/UploadProjectItem.qml" line="163"/>
        <source>Account Disabled</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/Workshop/upload/UploadProjectItem.qml" line="166"/>
        <source>Service ReadOnly</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/Workshop/upload/UploadProjectItem.qml" line="169"/>
        <source>Account Not Featured</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/Workshop/upload/UploadProjectItem.qml" line="172"/>
        <source>Administrator OK</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/Workshop/upload/UploadProjectItem.qml" line="175"/>
        <source>Content Version</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/Workshop/upload/UploadProjectItem.qml" line="178"/>
        <source>Try Another CM</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/Workshop/upload/UploadProjectItem.qml" line="181"/>
        <source>Password Required T oKick Session</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/Workshop/upload/UploadProjectItem.qml" line="184"/>
        <source>Already Logged In Elsewhere</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/Workshop/upload/UploadProjectItem.qml" line="187"/>
        <source>Suspended</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/Workshop/upload/UploadProjectItem.qml" line="190"/>
        <source>Cancelled</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/Workshop/upload/UploadProjectItem.qml" line="193"/>
        <source>Data Corruption</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/Workshop/upload/UploadProjectItem.qml" line="196"/>
        <source>Disk Full</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/Workshop/upload/UploadProjectItem.qml" line="199"/>
        <source>Remote Call Failed</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/Workshop/upload/UploadProjectItem.qml" line="202"/>
        <source>Password Unset</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/Workshop/upload/UploadProjectItem.qml" line="205"/>
        <source>External Account Unlinked</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/Workshop/upload/UploadProjectItem.qml" line="208"/>
        <source>PSN Ticket Invalid</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/Workshop/upload/UploadProjectItem.qml" line="211"/>
        <source>External Account Already Linked</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/Workshop/upload/UploadProjectItem.qml" line="214"/>
        <source>Remote File Conflict</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/Workshop/upload/UploadProjectItem.qml" line="217"/>
        <source>Illegal Password</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/Workshop/upload/UploadProjectItem.qml" line="220"/>
        <source>Same As Previous Value</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/Workshop/upload/UploadProjectItem.qml" line="223"/>
        <source>Account Logon Denied</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/Workshop/upload/UploadProjectItem.qml" line="226"/>
        <source>Cannot Use Old Password</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/Workshop/upload/UploadProjectItem.qml" line="229"/>
        <source>Invalid Login AuthCode</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/Workshop/upload/UploadProjectItem.qml" line="232"/>
        <source>Account Logon Denied No Mail</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/Workshop/upload/UploadProjectItem.qml" line="235"/>
        <source>Hardware Not Capable Of IPT</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/Workshop/upload/UploadProjectItem.qml" line="238"/>
        <source>IPT Init Error</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/Workshop/upload/UploadProjectItem.qml" line="241"/>
        <source>Parental Control Restricted</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/Workshop/upload/UploadProjectItem.qml" line="244"/>
        <source>Facebook Query Error</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/Workshop/upload/UploadProjectItem.qml" line="247"/>
        <source>Expired Login Auth Code</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/Workshop/upload/UploadProjectItem.qml" line="250"/>
        <source>IP Login Restriction Failed</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/Workshop/upload/UploadProjectItem.qml" line="253"/>
        <source>Account Locked Down</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/Workshop/upload/UploadProjectItem.qml" line="256"/>
        <source>Account Logon Denied Verified Email Required</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/Workshop/upload/UploadProjectItem.qml" line="259"/>
        <source>No MatchingURL</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/Workshop/upload/UploadProjectItem.qml" line="262"/>
        <source>Bad Response</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/Workshop/upload/UploadProjectItem.qml" line="265"/>
        <source>Require Password ReEntry</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/Workshop/upload/UploadProjectItem.qml" line="268"/>
        <source>Value Out Of Range</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/Workshop/upload/UploadProjectItem.qml" line="271"/>
        <source>Unexpecte Error</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/Workshop/upload/UploadProjectItem.qml" line="274"/>
        <source>Disabled</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/Workshop/upload/UploadProjectItem.qml" line="277"/>
        <source>Invalid CEG Submission</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/Workshop/upload/UploadProjectItem.qml" line="280"/>
        <source>Restricted Device</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/Workshop/upload/UploadProjectItem.qml" line="283"/>
        <source>Region Locked</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/Workshop/upload/UploadProjectItem.qml" line="286"/>
        <source>Rate Limit Exceeded</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/Workshop/upload/UploadProjectItem.qml" line="289"/>
        <source>Account Login Denied Need Two Factor</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/Workshop/upload/UploadProjectItem.qml" line="292"/>
        <source>Item Deleted</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/Workshop/upload/UploadProjectItem.qml" line="295"/>
        <source>Account Login Denied Throttle</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/Workshop/upload/UploadProjectItem.qml" line="298"/>
        <source>Two Factor Code Mismatch</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/Workshop/upload/UploadProjectItem.qml" line="301"/>
        <source>Two Factor Activation Code Mismatch</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/Workshop/upload/UploadProjectItem.qml" line="304"/>
        <source>Account Associated To Multiple Partners</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/Workshop/upload/UploadProjectItem.qml" line="307"/>
        <source>Not Modified</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/Workshop/upload/UploadProjectItem.qml" line="310"/>
        <source>No Mobile Device</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/Workshop/upload/UploadProjectItem.qml" line="313"/>
        <source>Time Not Synced</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/Workshop/upload/UploadProjectItem.qml" line="316"/>
        <source>Sms Code Failed</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/Workshop/upload/UploadProjectItem.qml" line="319"/>
        <source>Account Limit Exceeded</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/Workshop/upload/UploadProjectItem.qml" line="322"/>
        <source>Account Activity Limit Exceeded</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/Workshop/upload/UploadProjectItem.qml" line="325"/>
        <source>Phone Activity Limit Exceeded</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/Workshop/upload/UploadProjectItem.qml" line="328"/>
        <source>Refund To Wallet</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/Workshop/upload/UploadProjectItem.qml" line="331"/>
        <source>Email Send Failure</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/Workshop/upload/UploadProjectItem.qml" line="334"/>
        <source>Not Settled</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/Workshop/upload/UploadProjectItem.qml" line="337"/>
        <source>Need Captcha</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/Workshop/upload/UploadProjectItem.qml" line="340"/>
        <source>GSLT Denied</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/Workshop/upload/UploadProjectItem.qml" line="343"/>
        <source>GS Owner Denied</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/Workshop/upload/UploadProjectItem.qml" line="346"/>
        <source>Invalid Item Type</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/Workshop/upload/UploadProjectItem.qml" line="349"/>
        <source>IP Banned</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/Workshop/upload/UploadProjectItem.qml" line="352"/>
        <source>GSLT Expired</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/Workshop/upload/UploadProjectItem.qml" line="355"/>
        <source>Insufficient Funds</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/Workshop/upload/UploadProjectItem.qml" line="358"/>
        <source>Too Many Pending</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/Workshop/upload/UploadProjectItem.qml" line="361"/>
        <source>No Site Licenses Found</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/Workshop/upload/UploadProjectItem.qml" line="364"/>
        <source>WG Network Send Exceeded</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/Workshop/upload/UploadProjectItem.qml" line="367"/>
        <source>Account Not Friends</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/Workshop/upload/UploadProjectItem.qml" line="370"/>
        <source>Limited User Account</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/Workshop/upload/UploadProjectItem.qml" line="373"/>
        <source>Cant Remove Item</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/Workshop/upload/UploadProjectItem.qml" line="376"/>
        <source>Account Deleted</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/Workshop/upload/UploadProjectItem.qml" line="379"/>
        <source>Existing User Cancelled License</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/Workshop/upload/UploadProjectItem.qml" line="382"/>
        <source>Community Cooldown</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/Workshop/upload/UploadProjectItem.qml" line="446"/>
        <source>Status:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/Workshop/upload/UploadProjectItem.qml" line="463"/>
        <source>Upload Progress: </source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>Workshop</name>
    <message>
        <location filename="../qml/Workshop/Workshop.qml" line="133"/>
        <source>Loading</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/Workshop/Workshop.qml" line="144"/>
        <source>Download now!</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/Workshop/Workshop.qml" line="149"/>
        <source>Downloading...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/Workshop/Workshop.qml" line="156"/>
        <source>Details</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/Workshop/Workshop.qml" line="178"/>
        <source>Open In Steam</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/Workshop/Workshop.qml" line="252"/>
        <source>Search for Wallpaper and Widgets...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/Workshop/Workshop.qml" line="288"/>
        <source>Ranked By Vote</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/Workshop/Workshop.qml" line="291"/>
        <source>Publication Date</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/Workshop/Workshop.qml" line="294"/>
        <source>Ranked By Trend</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/Workshop/Workshop.qml" line="297"/>
        <source>Favorited By Friends</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/Workshop/Workshop.qml" line="300"/>
        <source>Created By Friends</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/Workshop/Workshop.qml" line="303"/>
        <source>Created By Followed Users</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/Workshop/Workshop.qml" line="306"/>
        <source>Not Yet Rated</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/Workshop/Workshop.qml" line="309"/>
        <source>Total VotesAsc</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/Workshop/Workshop.qml" line="312"/>
        <source>Votes Up</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/Workshop/Workshop.qml" line="315"/>
        <source>Total Unique Subscriptions</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>WorkshopItem</name>
    <message>
        <location filename="../qml/Workshop/WorkshopItem.qml" line="184"/>
        <source>Download</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/Workshop/WorkshopItem.qml" line="302"/>
        <source>Successfully subscribed to Workshop Item!</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/Workshop/WorkshopItem.qml" line="412"/>
        <source>Download complete!</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>XMLNewsfeed</name>
    <message>
        <location filename="../qml/Community/XMLNewsfeed.qml" line="53"/>
        <source>News &amp; Patchnotes</source>
        <translation type="unfinished">Neuigkeiten &amp; Patchnotes</translation>
    </message>
</context>
<context>
    <name>main</name>
    <message>
        <source>ScreenPlay - Double click to change you settings.</source>
        <translation type="obsolete">ScreenPlay - Doppelklicke, um deine Einstellungen zu ändern.</translation>
    </message>
    <message>
        <source>Open ScreenPlay</source>
        <translation type="obsolete">Öffne ScreenPlay</translation>
    </message>
    <message>
        <source>Mute all</source>
        <translation type="obsolete">Alles stummschalten</translation>
    </message>
    <message>
        <source>Unmute all</source>
        <translation type="obsolete">Alle Stummschaltungen aufheben</translation>
    </message>
    <message>
        <source>Pause all</source>
        <translation type="obsolete">Alles pausieren</translation>
    </message>
    <message>
        <source>Play all</source>
        <translation type="obsolete">Alles abspielen</translation>
    </message>
    <message>
        <source>Quit</source>
        <translation type="obsolete">Beenden</translation>
    </message>
</context>
</TS>
